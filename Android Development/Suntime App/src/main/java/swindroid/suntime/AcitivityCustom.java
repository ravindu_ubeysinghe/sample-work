package swindroid.suntime;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;

import java.io.BufferedOutputStream;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.TimeZone;

public class AcitivityCustom extends Activity {

    private Spinner spinner;
    private EditText longitudeInput, latitudeInput, nameInput;
    private ArrayList<String> tzList;
    private String name, latitude, longitude, timezone, FILENAME = "custom_list.txt";
    private FileOutputStream fos;
    private BufferedOutputStream bos;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_custom);
        populateSpinner();
    }

    private void obtainData() {
        nameInput = (EditText)findViewById(R.id.name);
        longitudeInput = (EditText)findViewById(R.id.longitude);
        latitudeInput = (EditText)findViewById(R.id.latitude);

        name = nameInput.getText().toString();
        longitude = longitudeInput.getText().toString();
        latitude = latitudeInput.getText().toString();
        timezone = String.valueOf(spinner.getSelectedItem());
    }

    private void populateSpinner() {
        spinner = (Spinner)findViewById(R.id.timezone);
        tzList = new ArrayList<String>();
        String [] tzs = TimeZone.getAvailableIDs();
        for(String id:tzs) {
            TimeZone tz = TimeZone.getTimeZone(id);
            int offset = tz.getRawOffset()/1000;
            int hour = offset/3600;
            int minutes = (offset % 3600)/60;
            tzList.add(String.format("(GMT%+d:%02d) %s", hour, minutes, id));
        }

        System.out.println(tzList.size());

        //Create an ArrayAdapter
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, tzList);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_acitivity_custom, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void saveToTheFile(View view) {
        obtainData();
        String temp[] = timezone.split("\\s+");
        System.out.println(name + ",-" + latitude + "," + longitude + "," + temp[1]);
        String output = name + "," + latitude + "," + longitude + "," + temp[1];
        try {
            fos = openFileOutput(FILENAME, Context.MODE_PRIVATE);
            fos.write(output.getBytes());
            fos.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        Intent i = new Intent(AcitivityCustom.this, CityActivity.class);
        startActivity(i);
    }
}
