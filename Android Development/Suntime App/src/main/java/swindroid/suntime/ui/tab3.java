package swindroid.suntime.ui;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import android.app.ActionBar;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.DatePickerDialog.OnDateSetListener;
import android.app.Fragment;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.InputType;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import swindroid.suntime.R;

/**
 * Created by Ravindu on 06-Oct-15.
 */
public class tab3 extends Fragment{
    static final int DIALOG_ID = 0;

    private DatePickerDialog fromDatePickerDialog;
    private DatePickerDialog toDatePickerDialog;

    private SimpleDateFormat dateFormatter;
    private EditText fDate;
    private EditText tDate;
    private String currentView = "fDate";

    private String startDate;
    private String endDate;
    private TableLayout tMain;
    private TextView dateV, riseV, setV;
    private TableRow tRow;

    private String city, latitude, longitude, timezone;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.tab3, container, false);

        Intent i = getActivity().getIntent();
        Bundle bundle = i.getBundleExtra("data");
        city = bundle.getString("city");
        latitude = bundle.getString("latitude");
        longitude = bundle.getString("longitude");
        timezone = bundle.getString("timezone");

        fDate = (EditText) v.findViewById(R.id.fDate);
        tDate = (EditText) v.findViewById(R.id.tDate);
        dateV = (TextView) v.findViewById(R.id.rowDate);
        riseV = (TextView) v.findViewById(R.id.rowRise);
        setV = (TextView) v.findViewById(R.id.rowSet);
        tMain = (TableLayout) v.findViewById(R.id.tList);
        tMain.setColumnStretchable(0, true);
        tMain.setColumnStretchable(1, true);
        tMain.setColumnStretchable(2, true);
        return v;

    }

    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        fDate.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                showDatePicker();
                currentView = "fDate";
            }
        });

        tDate.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                showDatePicker();
                currentView = "tDate";
            }
        });
    }

    private void showDatePicker() {
        DatePickerFragment date = new DatePickerFragment();
        /**
         * Set Up Current Date Into dialog
         */
        Calendar calender = Calendar.getInstance();
        Bundle args = new Bundle();
        args.putInt("year", calender.get(Calendar.YEAR));
        args.putInt("month", calender.get(Calendar.MONTH));
        args.putInt("day", calender.get(Calendar.DAY_OF_MONTH));
        date.setArguments(args);
        /**
         * Set Call back to capture selected date
         */
        date.setCallBack(ondate);
        date.show(getFragmentManager(), "Date Picker");
    }

    OnDateSetListener ondate = new OnDateSetListener() {

        public void onDateSet(DatePicker view, int year, int monthOfYear,
                              int dayOfMonth) {
            if(currentView.equals("fDate")) {
                fDate.setText(String.valueOf(dayOfMonth) + "-" + String.valueOf(monthOfYear + 1)
                        + "-" + String.valueOf(year));

                fDate.setEnabled(false); fDate.setInputType(InputType.TYPE_NULL);

                //Setting the start date instance var
                startDate = String.valueOf(dayOfMonth) + "-" + String.valueOf(monthOfYear + 1)
                        + "-" + String.valueOf(year);
            }
            else if(currentView.equals("tDate")){
                tDate.setText(String.valueOf(dayOfMonth) + "-" + String.valueOf(monthOfYear + 1)
                        + "-" + String.valueOf(year));

                tDate.setEnabled(false); tDate.setInputType(InputType.TYPE_NULL);

                //Setting the start date instance var
                endDate = String.valueOf(dayOfMonth) + "-" + String.valueOf(monthOfYear + 1)
                        + "-" + String.valueOf(year);
            }

            if(!fDate.getText().toString().equals("") && !tDate.getText().toString().equals("")){
                Toast.makeText(getActivity(), "Both have been selected",
                        Toast.LENGTH_LONG).show();
                try {
                    populateTheList();
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }
        }
    };

    private void populateTheList() throws ParseException {
        SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
        DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
        Date startDatel = formatter.parse(startDate);
        Date endDatel = formatter.parse(endDate);

        Calendar start = Calendar.getInstance();
        start.setTime(startDatel);
        Calendar end = Calendar.getInstance();
        end.setTime(endDatel);

        //Setup the headers

        tRow = new TableRow(getActivity());
        dateV = new TextView(getActivity());
        riseV = new TextView(getActivity());
        setV = new TextView(getActivity());

        dateV.setText("Date");
        dateV.setTextSize(30);
        dateV.setGravity(Gravity.CENTER);

        riseV.setText("Rise Time");
        riseV.setTextSize(30);
        riseV.setGravity(Gravity.CENTER);

        setV.setText("Set Time");
        setV.setTextSize(30);
        setV.setGravity(Gravity.CENTER);

        tRow.addView(dateV);
        tRow.addView(riseV);
        tRow.addView(setV);

        tMain.addView(tRow);

        String currentDate = startDate;
        for (Date date = start.getTime(); start.before(end); start.add(Calendar.DATE, 1), date = start.getTime())
        {
            String newDate = dateFormat.format(date);
            String result = tab1.getSSSR(city, latitude, longitude, timezone, newDate);
            String[] resultArray = result.split("-");
            String sunriseOutput = resultArray[0];
            String sunsetOutput = resultArray[1];

            tRow = new TableRow(getActivity());
            dateV = new TextView(getActivity());
            riseV = new TextView(getActivity());
            setV = new TextView(getActivity());

            dateV.setText(newDate);
            dateV.setTextSize(15);
            dateV.setGravity(Gravity.CENTER);

            riseV.setText(sunriseOutput);
            riseV.setTextSize(15);
            riseV.setGravity(Gravity.CENTER);

            setV.setText(sunsetOutput);
            setV.setTextSize(15);
            setV.setGravity(Gravity.CENTER);

            tRow.addView(dateV);
            tRow.addView(riseV);
            tRow.addView(setV);

            tMain.addView(tRow);

        }
    }

}
