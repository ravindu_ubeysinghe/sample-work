package swindroid.suntime.ui;

import android.app.Fragment;
import android.app.FragmentManager;
import android.support.v13.app.FragmentPagerAdapter;

import java.util.List;

/**
 * Created by Ravindu on 06-Oct-15.
 */
public class PageAdapter extends FragmentPagerAdapter {
    private List<Fragment> fragments;

    public PageAdapter(FragmentManager fm){
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        Fragment frag = null;
        if(position == 0){
            frag = new tab1();
        }else if(position == 1){
            frag = new tab2();
        }else if(position == 2){
            frag = new tab3();
        }
        return frag;
    }

    @Override
    public int getCount() {
        return 3;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        if(position == 0){
            return "Information";
        }
        else if(position == 1){
            return "Map View";
        }else if(position == 2){
            return "Predictions";
        }
        return super.getPageTitle(position);
    }
}
