package swindroid.suntime.ui;

import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import swindroid.suntime.R;
import swindroid.suntime.calc.AstronomicalCalendar;
import swindroid.suntime.calc.GeoLocation;

/**
 * Created by Ravindu on 05-Oct-15.
 */
public class tab1 extends Fragment {

    private String city;
    private double latitude;
    private double longitude;
    private String timezone;
    private String sunRise;
    private String sunSet;
    private String Date;
    private View v;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.main, container, false);
        initializeUI();
        return v;
    }

    public void onStart(){
        super.onStart();
    }

    private void initializeUI()
    {
        Intent i = getActivity().getIntent();
        Bundle bundle = i.getBundleExtra("data");
        city = bundle.getString("city");
        latitude = Double.parseDouble(bundle.getString("latitude"));
        longitude = Double.parseDouble(bundle.getString("longitude"));
        timezone = bundle.getString("timezone");
        DatePicker dp = (DatePicker) v.findViewById(R.id.datePicker);
        TextView cityTv = (TextView)v.findViewById(R.id.locationTV);
        cityTv.setText(city);
        Calendar cal = Calendar.getInstance();
        int year = cal.get(Calendar.YEAR);
        int month = cal.get(Calendar.MONTH);
        int day = cal.get(Calendar.DAY_OF_MONTH);
        Date = day + "/" + month + "/" + year;
        dp.init(year, month, day, dateChangeHandler); // setup initial values and reg. handler
        updateTime(year, month, day);
    }

    private void updateTime(int year, int monthOfYear, int dayOfMonth)
    {
        TimeZone tz = TimeZone.getTimeZone(timezone);
        GeoLocation geolocation = new GeoLocation(city, latitude, longitude, tz);
        AstronomicalCalendar ac = new AstronomicalCalendar(geolocation);
        ac.getCalendar().set(year, monthOfYear, dayOfMonth);
        Date srise = ac.getSunrise();
        Date sset = ac.getSunset();

        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");

        TextView sunriseTV = (TextView) v.findViewById(R.id.sunriseTimeTV);
        TextView sunsetTV = (TextView) v.findViewById(R.id.sunsetTimeTV);
        Log.d("SUNRISE Unformatted", srise + "");

        sunRise = sdf.format(srise);
        sunSet = sdf.format(sset);

        sunriseTV.setText(sdf.format(srise));
        sunsetTV.setText(sdf.format(sset));
    }

    DatePicker.OnDateChangedListener dateChangeHandler = new DatePicker.OnDateChangedListener()
    {
        public void onDateChanged(DatePicker dp, int year, int monthOfYear, int dayOfMonth)
        {
            updateTime(year, monthOfYear, dayOfMonth);
        }
    };

    @Override
    public String toString() {
        String output = "";
        output = "Sunset/Sunrise Info - " + "City: " + city + " Date: " + Date + " Sun Rise: " + sunRise + " Sun Set: " + sunSet;
        return output;
    }

    public static String getSSSR(String city, String latitude, String longitude, String timezone){

        String city_l = city;
        Double latitude_l = Double.parseDouble(latitude);
        Double longitude_l = Double.parseDouble(longitude);
        Calendar cal = Calendar.getInstance();
        int year = cal.get(Calendar.YEAR);
        int month = cal.get(Calendar.MONTH);
        int day = cal.get(Calendar.DAY_OF_MONTH);

        String date = day + "/" + month + "/" + year;

        TimeZone tz = TimeZone.getTimeZone(timezone);
        GeoLocation geolocation = new GeoLocation(city_l, latitude_l, longitude_l, tz);
        AstronomicalCalendar ac = new AstronomicalCalendar(geolocation);
        ac.getCalendar().set(year, month, day);
        Date srise = ac.getSunrise();
        Date sset = ac.getSunset();

        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");

        String output = "";
        output = "Sunset/Sunrise Info - " + "City: " + city + " Date: " + date+ " Sun Rise: " + sdf.format(srise) + " Sun Set: " + sdf.format(sset);
        return output;
    }

    //Overload
    public static String getSSSR(String city, String latitude, String longitude, String timezone, String datel){

        String city_l = city;
        Double latitude_l = Double.parseDouble(latitude);
        Double longitude_l = Double.parseDouble(longitude);
        String[] date_elements = datel.split("-");
        int year = Integer.parseInt(date_elements[2]);
        int month = Integer.parseInt(date_elements[1]);
        int day = Integer.parseInt(date_elements[0]);

        String date = day + "/" + month + "/" + year;

        TimeZone tz = TimeZone.getTimeZone(timezone);
        GeoLocation geolocation = new GeoLocation(city_l, latitude_l, longitude_l, tz);
        AstronomicalCalendar ac = new AstronomicalCalendar(geolocation);
        ac.getCalendar().set(year, month, day);
        Date srise = ac.getSunrise();
        Date sset = ac.getSunset();

        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");

        String output = "";
        output = sdf.format(srise) + "-" + sdf.format(sset);
        return output;
    }
}
