package swindroid.suntime.ui;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import swindroid.suntime.R;
import swindroid.suntime.calc.AstronomicalCalendar;
import swindroid.suntime.calc.GeoLocation;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.DatePicker;
import android.widget.DatePicker.OnDateChangedListener;
import android.widget.TextView;

public class Main extends Activity 
{
	private String city;
	private double latitude;
	private double longitude;
	private String timezone;

    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) 
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        initializeUI();
    }

	private void initializeUI()
	{
		Intent i = getIntent();
		Bundle bundle = i.getBundleExtra("data");
		city = bundle.getString("city");
		latitude = Double.parseDouble(bundle.getString("latitude"));
		longitude = Double.parseDouble(bundle.getString("longitude"));
		timezone = bundle.getString("timezone");
		DatePicker dp = (DatePicker) findViewById(R.id.datePicker);
		TextView cityTv = (TextView)findViewById(R.id.locationTV);
		cityTv.setText(city);
		Calendar cal = Calendar.getInstance();
		int year = cal.get(Calendar.YEAR);
		int month = cal.get(Calendar.MONTH);
		int day = cal.get(Calendar.DAY_OF_MONTH);
		dp.init(year,month,day,dateChangeHandler); // setup initial values and reg. handler
		updateTime(year, month, day);
	}
    
	private void updateTime(int year, int monthOfYear, int dayOfMonth)
	{
		TimeZone tz = TimeZone.getTimeZone(timezone);
		GeoLocation geolocation = new GeoLocation(city, latitude, longitude, tz);
		AstronomicalCalendar ac = new AstronomicalCalendar(geolocation);
		ac.getCalendar().set(year, monthOfYear, dayOfMonth);
		Date srise = ac.getSunrise();
		Date sset = ac.getSunset();
		
		SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
		
		TextView sunriseTV = (TextView) findViewById(R.id.sunriseTimeTV);
		TextView sunsetTV = (TextView) findViewById(R.id.sunsetTimeTV);
		Log.d("SUNRISE Unformatted", srise+"");
		
		sunriseTV.setText(sdf.format(srise));
		sunsetTV.setText(sdf.format(sset));		
	}
	
	OnDateChangedListener dateChangeHandler = new OnDateChangedListener()
	{
		public void onDateChanged(DatePicker dp, int year, int monthOfYear, int dayOfMonth)
		{
			updateTime(year, monthOfYear, dayOfMonth);
		}	
	};

	public static String getSSSR(String city, String latitude, String longitude, String timezone){

		String city_l = city;
		Double latitude_l = Double.parseDouble(latitude);
		Double longitude_l = Double.parseDouble(longitude);
		Calendar cal = Calendar.getInstance();
		int year = cal.get(Calendar.YEAR);
		int month = cal.get(Calendar.MONTH);
		int day = cal.get(Calendar.DAY_OF_MONTH);

		String date = day + "/" + month + "/" + year;

		TimeZone tz = TimeZone.getTimeZone(timezone);
		GeoLocation geolocation = new GeoLocation(city_l, latitude_l, longitude_l, tz);
		AstronomicalCalendar ac = new AstronomicalCalendar(geolocation);
		ac.getCalendar().set(year, month, day);
		Date srise = ac.getSunrise();
		Date sset = ac.getSunset();

		SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");

		String output = "";
		output = "Sunset/Sunrise Info - " + "City: " + city + " Date: " + date+ " Sun Rise: " + sdf.format(srise) + " Sun Set: " + sdf.format(sset);
		return output;
	}
	
}