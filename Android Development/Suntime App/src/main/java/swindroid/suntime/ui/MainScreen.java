package swindroid.suntime.ui;

import android.app.ActionBar;
import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.ShareActionProvider;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;
import java.util.Vector;

import swindroid.suntime.R;

public class MainScreen extends Activity {
    String[] actions = {"Information", "Map View", "Predictions"};
    ArrayAdapter<String> adapter;
    Intent mShareIntent;
    String ssinfo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_screen);
        sendShareIntent();

        //Intent stuff
        Intent i = getIntent();
        Bundle bundle = i.getBundleExtra("data");
        String city = bundle.getString("city");
        String latitude = bundle.getString("latitude");
        String longitude = bundle.getString("longitude");
        String timezone = bundle.getString("timezone");
        ssinfo = tab1.getSSSR(city,latitude,longitude,timezone);

        adapter = new ArrayAdapter<String>(this, android.R.layout.simple_dropdown_item_1line, actions);
        ActionBar bar = getActionBar();
        bar.setNavigationMode(ActionBar.NAVIGATION_MODE_LIST);
        ActionBar.OnNavigationListener listener = new ActionBar.OnNavigationListener() {
            @Override
            public boolean onNavigationItemSelected(int itemPosition, long itemId) {
                Fragment fragment = null;
                FragmentTransaction fragmentTransaction = getFragmentManager()
                        .beginTransaction();
                switch (itemPosition) {
                    case 0:
                        fragment = new tab1();
                        if(fragment == null){fragmentTransaction.add(android.R.id.content, fragment, "TAB_1");}
                        else {fragmentTransaction.replace(android.R.id.content, fragment);}
                        sendShareIntent();
                        invalidateOptionsMenu();
                        System.out.println("tab1 initiated");
                        break;

                    case 1:
                        fragment = new tab2();
                        if(fragment == null){fragmentTransaction.add(android.R.id.content, fragment, "TAB_2");}
                        else {fragmentTransaction.replace(android.R.id.content, fragment);}
                        sendShareIntent();
                        invalidateOptionsMenu();
                        break;

                    case 2:
                        fragment = new tab3();
                        if(fragment == null){fragmentTransaction.add(android.R.id.content, fragment, "TAB_3");}
                        else {fragmentTransaction.replace(android.R.id.content, fragment);}
                        sendShareIntent();
                        invalidateOptionsMenu();
                        break;

                    default:
                        break;
                }

                //fragmentTransaction.replace(android.R.id.content, fragment);
                fragmentTransaction.commit();
                return true;
            }
        };

        getActionBar().setListNavigationCallbacks(adapter, listener);

    }

    public void sendShareIntent(){
        TextView cityTv = (TextView)findViewById(R.id.locationTV);
        DatePicker dp = (DatePicker) findViewById(R.id.datePicker);
        TextView sunriseTV = (TextView) findViewById(R.id.sunriseTimeTV);
        TextView sunsetTV = (TextView) findViewById(R.id.sunsetTimeTV);
        //String shareString = ssinfo;

        String shareString;
        tab1 t1 = (tab1)getFragmentManager().findFragmentByTag("TAG_1");
        if(t1 != null) {
            shareString = t1.toString();
        }else{
            shareString = ssinfo;
        }
        System.out.println(shareString);

        mShareIntent = new Intent();
        mShareIntent.setAction(Intent.ACTION_SEND);
        mShareIntent.setType("text/plain");
        mShareIntent.putExtra(Intent.EXTRA_TEXT, shareString);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main_screen, menu);
        // Locate MenuItem with ShareActionProvider
        MenuItem item = menu.findItem(R.id.menu_item_share);

        // Fetch and store ShareActionProvider
        ShareActionProvider mShare = (ShareActionProvider) item.getActionProvider();

        Intent shareIntent = new Intent(Intent.ACTION_SEND);
        shareIntent.setAction(Intent.ACTION_SEND);
        shareIntent.setType("text/plain");

        mShare.setShareIntent(mShareIntent);

        return true;
    }



    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
