package swindroid.suntime;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

import swindroid.suntime.ui.Main;
import swindroid.suntime.ui.MainScreen;

public class CityActivity extends Activity {

    //creating array lists for each detail given in the text file
    private ArrayList<String> cityListMain = new ArrayList<String>();
    private ArrayList<String> latitude = new ArrayList<String>();
    private ArrayList<String> longitude = new ArrayList<String>();
    private ArrayList<String> capital = new ArrayList<String>();
    private String FILENAME = "custom_list.txt";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_city);

        createListView();
        itemClick();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_city, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    public void itemClick()
    {
        ListView lv = (ListView)findViewById(R.id.cityList);
        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                TextView tv = (TextView) view;
                String cityTemp = tv.getText().toString();
                String latitudeTemp = latitude.get(position);
                String longitudeTemp = longitude.get(position);
                String timezone = capital.get(position);

                Bundle bundle = new Bundle();
                bundle.putString("city", cityTemp);
                bundle.putString("latitude", latitudeTemp);
                bundle.putString("longitude", longitudeTemp);
                bundle.putString("timezone", timezone);

                Intent i = new Intent(CityActivity.this, MainScreen.class);
                i.putExtra("data", bundle);
                startActivity(i);
            }
        });

    }


    public void createListView()
    {
        separateList();

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, R.layout.single_item, cityListMain);

        ListView lv = (ListView)findViewById(R.id.cityList);
        lv.setAdapter(adapter);
    }

    //Function responsible for fetching all the lines in the text file and separating them out to arraylists
    public void separateList()
    {
        ArrayList<String> list = readFile();

        int size = list.size();
        String[] temp = new String[4];
        //System.out.println(list.get(1));

        for(int i=0; i<size; i++){
            temp = list.get(i).split(",");
            cityListMain.add(temp[0]);
            latitude.add(temp[1]);
            longitude.add(temp[2]);
            capital.add(temp[3]);
        }
    }

    public ArrayList<String> readFile() {
        ArrayList<String> cityList = new ArrayList<String>();


        BufferedReader br = null;
        try {
            br = new BufferedReader(new InputStreamReader(getAssets().open("au_locations.txt")));
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("error read file");
        }
        String city;

        // Read file line by line and print on the console
        try {
            while ((city = br.readLine()) != null)   {
                cityList.add(city);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            br.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        //Read from the custom locations file
        if(fileExists(FILENAME)) {
            FileInputStream fis = null;
            try {
                fis = openFileInput(FILENAME);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
            BufferedReader br1 = new BufferedReader(new InputStreamReader(fis));

            // Read file line by line and print on the console
            try {
                while ((city = br1.readLine()) != null) {
                    System.out.println("once");
                    cityList.add(city);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }

            try {
                br1.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        return cityList;
    }

    public void openCustomLocationSelection(View view) {
        Intent i = new Intent(CityActivity.this, AcitivityCustom.class);
        startActivity(i);
    }

    public boolean fileExists(String fname){
        File file = getBaseContext().getFileStreamPath(fname);
        return file.exists();
    }
}
