<?php
	/*
		Template Name: Approve Page Don't Use
	*/
	
	$offer_id = $_GET["offerid"];
	$action = $_GET["action"];
	
	
	require_once('resource_settings.php');
	
	$conn = new mysqli($servername, $username, $password, $dbname);
	// Check connection
	if ($conn->connect_error) {
		echo pll__('Database Connection failed');
	}	
	
	if($action == "approve")
	{
		$update_approve_sql = "UPDATE ResourceBacking SET approved = 1 WHERE resource_id = '$offer_id'";
		$update_approve_result = $conn->query($update_approve_sql);
		if($update_approve_result)
		{
			$offer_sql = "SELECT * FROM ResourceBacking WHERE resource_id = '$offer_id'";
			$offer_result = $conn->query($offer_sql);
			$offer_row = $offer_result->fetch_assoc();
			$receiver_email = $offer_row['backer_email'];
			$receiver_name = $offer_row['backer_name'];
			$offer = $offer_row['offer'];
			$project_id = $offer_row['project_id'];
			
			$project_sql = "SELECT * FROM wp_postmeta WHERE meta_key = 'ign_project_id' AND meta_value = '$project_id'";
			$project_result = $conn->query($project_sql);
			$project_row = $project_result->fetch_assoc();
			$post_id = $project_row['post_id'];
			
			$post_sql = "SELECT * FROM wp_posts WHERE ID = '$post_id'";
			$post_result = $conn->query($post_sql);
			$post_row = $post_result->fetch_assoc();
			$project_title = $post_row['post_title'];
			
			$alert_str = pll__('Alert');
			$offer_status_str = pll__('Offer Status');
			$dear = pll__('Dear');
			$pre_offer_3 = pll__('Your offer');
			$post_offer = pll__('has been accepted by the creator of the project');
			
			$default_footer_1 = pll__('Thank You');
			$default_footer_2 = pll__("This is an automated message, please don't reply");
			
			$subject = $alert_str.": ".$project_title." - ". $offer_status_str;
			$message_new = $dear. ", ". $receiver_name . ",\n\n". $pre_offer_3 .":\n". $offer ."\n". $post_offer ." \"".$project_title. "\"\n\n". $default_footer_1 .",\nSirius Crowd Funding.\n[". $default_footer_2 ."]";
		
			wp_mail($receiver_email, $subject, $message_new); //enable when ready to deploy
			
			$button_text = pll__('Unapprove');
			$status_text = pll__('Your offer has been accepted by the project creator');
			echo $status_text . "+" .  $button_text;
		}
		else
		{
			echo pll('Your offer is yet to be accepted');
		}
		
	}
	else if($action == "unapprove")
	{
		$update_unapprove_sql = "UPDATE ResourceBacking SET approved = 0 WHERE resource_id = '$offer_id'";
		$update_unapprove_result = $conn->query($update_unapprove_sql);
		if($update_unapprove_result)
		{
			$button_text = pll__('Approve');
			$status_text = pll__('Your offer is yet to be accepted');
			echo $status_text. "+" .$button_text;
		}
		else
		{
			echo pll__('Your offer has been accepted by the project creator');
		}
		
	}
	else if($action == "delete")
	{
		$delete_sql = "DELETE FROM ResourceBacking WHERE resource_id = '$offer_id'";
		$delete_result = $conn->query($delete_sql);
		if($delete_result)
		{
			echo pll__("Offer has been successfully deleted, please wait page is being refreshed..");
		}
		else
		{
			echo pll__("Sorry, The deletion was unsuccessful");
		}
	}
?>