<?php
	/*
		Template Name: Payment Page
	*/
	// Stripe singleton
	
	get_header();
	
	require_once('resource_settings.php');
	
	$conn = new mysqli($servername, $username, $password, $dbname);
	// Check connection
	if ($conn->connect_error) {
		echo"Connection failed: ";
	}	

	$project_id = $_POST['project_id'];
	$author_id = $_POST['author_id'];
	$post_id = $_POST['post_id'];
	if(isset($_POST['stripeToken']))
	{
		require_once '/home3/medviv/public_html/stage/wp-content/plugins/idcommerce/lib/Stripe.php';
		require_once '/home3/medviv/public_html/stage/wp-content/plugins/idcommerce/lib/Stripe/Charge.php';
		
		$error = "";
	
		
		// Set your secret key: remember to change this to your live secret key in production
		// See your keys here https://dashboard.stripe.com/account/apikeys
		Stripe::setApiKey("sk_test_nxl3ZghL1mcHT0mkx0MhyB8t");
		
		$payment_amount = $_POST['payment_amount'];
		$payment_description = $_POST['payment_description'];
		echo $project_description;
		
		// Get the credit card details submitted by the form
		$token = $_POST['stripeToken'];
		
		// Create the charge on Stripe's servers - this will charge the user's card
		/*try {
		$charge = Stripe_Charge::create(array(
		"amount" => $payment_amount, // amount in cents, again
		"currency" => "aud",
		"source" => $token,
		"description" => $payment_description)
		);
		}
		catch(Stripe_CardError $e) {
			$body = $e->getJsonBody();
			$err  = $body['error'];
			$error = $err['message'];
		}
		catch(Stripe_ApiConnectionError $e){
			$body = $e->getJsonBody();
			$err  = $body['error'];
			$error = $err['message'];
		}
		catch(Stripe_InvalidRequestError $e){
			$body = $e->getJsonBody();
			$err  = $body['error'];
			$error = $err['message'];
		}
		catch(Stripe_ApiError $e){
			$body = $e->getJsonBody();
			$err  = $body['error'];
			$error = $err['message'];
		}
		catch(Exception $e){
			$error = "Unfortunately, some error has occurred \n Please go back and make the payment again";
		}*/
		
		if($error == "")
		{
			updatePayment($conn, $project_id, $payment_amount);
			sendContactList($conn, $project_id, $author_id, $post_id);
?>
			<div style="margin:100px; font-size:150%">
			<p> <?php echo pll__('Your Payment Was Successful'); ?> </p>
			</br>
			<p> <?php echo pll__('You will be emailed a contact list of your backers shortly'); ?></p>
			</br>
			<p> <?php echo pll__('After this point contact details will be emailed to you upon each offer submission'); ?> </p>
			</br></br>
			<a href="<?php echo $_SERVER['HTTP_REFERER'] ?>"> <?php echo pll__('Go Back To The Project'); ?></a>
			</div>
<?php
		}
		else
		{
?>
			<div style="margin:100px; font-size:150%">
			<?php echo $error ?>
			</br></br>
			<a href="<?php echo $_SERVER['HTTP_REFERER'] ?>"> <?php echo pll__('Go Back To The Project'); ?></a>
			</div>
<?php
		}
	}
	else
	{
?>
	<div style="margin:100px; font-size:150%">
	<p><?php echo pll__("You don't have access to this page");?></p>
	</div>
<?php
	}
	
	function updatePayment($conn, $project_id, $payment)
	{
		$update_payment_sql = "INSERT INTO ResourcePayments(project_id, payment_made)
							   VALUES ('$project_id', 1)";
		$update_payment_result = $conn->query($update_payment_sql);
	}
	
	function sendContactList($conn, $project_id, $author_id, $post_id)
	{
		/*$user_id = get_current_user_id();
		$user_info = get_userdata($user_id);
		$backer_name = $user_info->first_name;
		$backer_email = $user_info->user_email;*/
		$project_title = get_the_title($post_id);
		$project_link = $_SERVER['HTTP_REFERER'];
		
		$receiver_email = get_the_author_meta( 'user_email', $author_id );
		$receiver_name = get_the_author_meta( 'first_name', $author_id );
		
		$list_sql = "SELECT DISTINCT backer_name, backer_email, offer FROM ResourceBacking WHERE project_id = '$project_id'";
		$list_result = $conn->query($list_sql);
		
		$subject = $project_title." - ". pll__("Resource/Service Backers' Contact List");
		
		$backer_name_str = pll__("Backer Name");
		$backer_email_str = pll__("Backer Email");
		$offer_str = pll__("Offer");
		
		while($single_row = $list_result->fetch_assoc()){
			
			$list .= "\n\n". $backer_name_str .": ". $single_row['backer_name'] . "  ". $backer_email_str .": ". $single_row['backer_email'] . "\n ". $offer_str .": " . $single_row['offer'];
		}
		
		$dear = pll__('Dear');
		$thanks = pll__('Thanks for the payment');
		$pre_list_1 = pll__('Following is the list of contact details of your backers along with their offers');
		$pre_list_2 = pll__("After this point you'll be notified via email with all the details when an offer is made");
		
		$default_footer_1 = pll__('Thank You');
		$default_footer_2 = pll__("This is an automated message, please don't reply");
		
		$start = $dear. ", " . $receiver_name . ", \n\n". $thanks .".\n".$pre_list_1.".\n". $pre_list_2 .".\n";
		
		$message_new = $start.$list."\n\n". $default_footer_1 .",\nSirius Crowd Funding.\n\n[". $default_footer_2 ."]";
		wp_mail($receiver_email, $subject, $message_new);
	}
	
	get_footer();
?>