<!--***********************************
	Custom Resource Backing Section
	Author: Ravindu Ubeysinghe
************************************-->
<script>
	//Function for approving and unapproving offers
	function ajax_submit(offer_id, action_i)
	{
		//alert("I'm clicked" + offer_id);
		var container = "response-" + offer_id;
		var button = "aubutton-" + offer_id;
		
		var action = action_i;
		var container_text = document.getElementById(container).innerHTML;
		var onclick_text_approve = "ajax_submit(" + offer_id + ", approve)";
		var onclick_text_unapprove = "ajax_submit(" + offer_id + ", unapprove)";
		
		//Determining the action based on the status line
		/*if(container_text == "Your offer is yet to be accepted")
		{
			action = "approve";
		}
		else if(container_text == "Your offer has been accepted by the project creator")
		{
			action = "unapprove";
		}*/
		
		
		//Creating the HTTP Request and getting the response from the php script
		var xmlhttp;
		if (window.XMLHttpRequest)
		{// code for IE7+, Firefox, Chrome, Opera, Safari
			xmlhttp=new XMLHttpRequest();
		}
		else
		{// code for IE6, IE5
			xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
		}
		xmlhttp.onreadystatechange=function()
		{
			if (xmlhttp.readyState==4 && xmlhttp.status == 200)
			{
				//alert(xmlhttp.responseText);
				var response = xmlhttp.responseText;
				var str_array = response.split("+");
				document.getElementById(container).innerHTML=str_array[0];
				document.getElementById(button).innerHTML=str_array[1];
				if(action == "approve")
				{
					location.reload(true);
					document.getElementById(button).setAttribute("onclick", onclick_text_unapprove);
				}
				else if(action == "unapprove")
				{
					location.reload(true);
					document.getElementById(button).setAttribute("onclick", onclick_text_approve);
				}
			}
		}
		//alert("http://stage.siriuscrowd.com/wp-content/themes/backer/resource_backing_approve.php?action=" + action + "&offerid=" + offer_id);
		xmlhttp.open("GET","http://stage.siriuscrowd.com/resource-backing-approve?action=" + action + "&offerid=" + offer_id,true);
		xmlhttp.send();
	}
	
	//Function to delete each offer
	function delete_offer(offer_id)
	{
		//alert("delete clicked");
		var container = "response-" + offer_id;
		var xmlhttp;
		if (window.XMLHttpRequest)
		{// code for IE7+, Firefox, Chrome, Opera, Safari
			xmlhttp=new XMLHttpRequest();
		}
		else
		{// code for IE6, IE5
			xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
		}
		xmlhttp.onreadystatechange=function()
		{
			if (xmlhttp.readyState==4 && xmlhttp.status == 200)
			{
				var text = xmlhttp.responseText
				document.getElementById(container).innerHTML= text;
				location.reload(true);
			}
		}
		xmlhttp.open("GET","http://stage.siriuscrowd.com/resource-backing-approve?action=delete&offerid=" + offer_id,true);
		xmlhttp.send();
	}
</script>
<?php
	//Getting the ignitiondeck project id (different from the wp post id)
	$project_id = get_post_meta( $post->ID, 'ign_project_id', true );
	//Getting the resource offer from the form below
	$offer = $_POST['resource'];
	//Getting the current user id
	$user_id = get_current_user_id();
	//Getting the current user
	$current_user = wp_get_current_user();
	
	/*
		CREATE TABLE ResourceBacking (
		resource_id INT(30) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
		user_id INT(30) NOT NULL,
		project_id INT(30) NOT NULL,
		offer VARCHAR(2000),
		approved TINYINT(1) NOT NULL,
		offer_date DATE,
		backer_name VARCHAR(255) NOT NULL,
		backer_email VARCHAR(255) NOT NULL
		)
		
		Database Creation script...
	*/
	
	/*
		CREATE TABLE ResourcePayments(
    	payment_id INT(30) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    	project_id INT(30) NOT NULL,
    	payment_made TINYINT(1) NOT NULL
		);
	
		Database Creation script...
	
	*/
	
	require_once('resource_settings.php');
	
	$conn = new mysqli($servername, $username, $password, $dbname);
	// Check connection, this will cause everything to not to show up on the project page except the tabs
	if ($conn->connect_error) {
		echo"Connection failed: ";
	}	
	
	//Create the table if ResourceBacking does not exist
	$table_exists = "SELECT table_name FROM information_schema.tables WHERE table_schema = '$dbname' AND table_name = 'ResourceBacking';";
	$table_results = $conn->query($table_exists);
	if($table_results->num_rows == 0)
	{
		$table_creation = "CREATE TABLE ResourceBacking (
		resource_id INT(30) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
		user_id INT(30) NOT NULL,
		project_id INT(30) NOT NULL,
		offer VARCHAR(2000),
		approved TINYINT(1) NOT NULL,
		offer_date DATE,
		backer_name VARCHAR(255) NOT NULL,
		backer_email VARCHAR(255) NOT NULL
		)";
		
		$table_creation_results = $conn->query($table_creation);
		if($table_creation_results->num_rows == 0)
		{
			echo("Table creation failed");
		}
	}
	
	//Create the table if ResourcePayments does not exist
	$table1_exists = "SELECT table_name FROM information_schema.tables WHERE table_schema = '$dbname' AND table_name = 'ResourcePayments';";
	$table1_results = $conn->query($table1_exists);
	if($table1_results->num_rows == 0)
	{
		$table1_creation = "CREATE TABLE ResourcePayments(
    	payment_id INT(30) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    	project_id INT(30) NOT NULL,
    	payment_made TINYINT(1) NOT NULL
		);";
		
		$table1_creation_results = $conn->query($table1_creation);
		if($table1_creation_results->num_rows == 0)
		{
			echo("Table creation failed");
		}
	}
	
	//Check if the current resource backing payment has been made
	$payment_sql = "SELECT payment_made FROM ResourcePayments WHERE project_id = '$project_id'";
	$payment_result = $conn->query($payment_sql);
	$pay_row = $payment_result->fetch_assoc();
	$payment_status = $pay_row['payment_made'];
	
	/************************
	Offer submission section
	************************/
	
	//Check if the offer is null, if so don't insert in to the database 
	//Secondly, check if the same offer has been made by the same user for the same project, if so reject the offer
	if($offer != ''){
		$check_duplicate = "SELECT offer FROM ResourceBacking WHERE user_id = '$user_id' AND project_id = '$project_id' AND offer = '$offer'";
		$result_duplicate = $conn->query($check_duplicate);
		$backer_name = $current_user->user_firstname.' '.$current_user->user_lastname;
		$backer_email = $current_user->user_email;
		
		if($result_duplicate->num_rows == 0)
		{
			$insert_resource = "INSERT INTO ResourceBacking (user_id, project_id, offer, approved, offer_date, backer_name, backer_email)
								VALUES ('$user_id', '$project_id', '$offer',0, NOW(), '$backer_name', '$backer_email')";
			if($conn->query($insert_resource) == TRUE)
			{
				//echo "successfully inserted";
				if($payment_status == 1){
					mail_author($offer, $post);
				}
			}
		}
		else
		{
			echo "<strong>". pll__("You've attempted to offer the same resource again, Please revise"). "!</strong></br></br>";
		}
	}
	
	//Function that sends to the author whenever a offer has been successfully saved.
	function mail_author($offer, $post)
	{
		$user_id = get_current_user_id();
		$user_info = get_userdata($user_id);
		$backer_name = $user_info->first_name;
		$backer_email = $user_info->user_email;
		$project_title = $post->post_title;
		$project_link = wp_get_shortlink();
		
		$receiver_email = get_the_author_meta( 'user_email', $post->post_author );
		$alert_str = pll__('Alert');
		$creator_email_subject = pll__('New Resource/Service Backing Offer');
		$subject = $alert_str.": ".$project_title." - ".$creator_email_subject;
		
		/*$message = "
			Backer Name: ". $backer_name . "<br />
			Backer E-mail: <a href=\"mailto:".$backer_email. "\" target=\"_blank\">".$backer_email ."</a><br />
			<br />
			The offer content is as follows:<br />
			<em>".$offer."</em><br />
			<br />
			You can view and approve the offer here: <a href=\"".$project_link."\" target=\"_blank\">".$project_title."</a><br />
			<br />
			Thank you,<br />
			Sirius Crowd Funding.<br />
			<br />
			[This is an automated message, please don&#39;t reply]</p>";
			
			HTML tags aren't necessary, have to use \n character instead*/
			
		$backer_name_str = pll__("Backer Name");
		$backer_email_str = pll__("Backer Email");
		$offer_str = pll__("Offer");
		
		$default_footer_1 = pll__('Thank You');
		$default_footer_2 = pll__("This is an automated message, please don't reply");
		
		$pre_offer = pll__('The offer content is as follows');
		$pre_offer_2 = pll__('You can view and approve the offer here');
		
		$message_new = 
		$backer_name_str.": ". $backer_name . "\n". $backer_email_str .": ".$backer_email. "\n\n".$pre_offer."\n".$offer."\n". $pre_offer_2 .": ".$project_link."\n\n". $default_footer_1 .",\nSirius Crowd Funding.[". $default_footer_2 ."]";
		
		wp_mail($receiver_email, $subject, $message_new);
		
	}
	
	/************************
	Offer listing section
	************************/
	
	//Get project goal
	$result_db = $wpdb->get_row( 
	"
	SELECT * 
	FROM wp_ign_products
	WHERE id='$project_id';
	"
	) or die(mysql_error());
	
	$project_goal = $result_db->goal;
	$payable_amount = $project_goal/100;
	$payable_amount = $payable_amount * 100;
	
	//Obtaining all the offers for the currently opened project to be displayed
	$offers = "SELECT * FROM ResourceBacking WHERE project_id = '$project_id'";
	$offers_result = $conn->query($offers);
	
	$current_user = wp_get_current_user();
	//List offers, if there are any
	if($offers_result->num_rows > 0)
	{
			$approved_sql = "SELECT COUNT(*) as count FROM ResourceBacking WHERE approved = 1  AND project_id = '$project_id'";
			$approved_result = $conn->query($approved_sql);
			$data= $approved_result->fetch_assoc();
			$approved_count = $data['count'];
			
			if($approved_count > 0 && ($payment_status == '' OR $payment_status == 0 ))
			{
			/*** payment button ***/
  
			
			//action="http://stage.siriuscrowd.com/wp-content/themes/backer/resource_payment.php"
			    if($current_user->ID == $post->post_author)
			    { 
					$locale_long = get_locale();
					$locale = substr($locale_long,0,2);
				?>
				<p><?php echo pll__('An amount of'). " A$" . $payable_amount/100 . " " . pll__('is payable to the administrator now!'); ?></p>
				<?php $permalink = get_permalink(pll_get_post(400)); ?>
				<form action="<?php echo $permalink ?>" method="POST" id="pay-form">
				<script
					src="https://checkout.stripe.com/checkout.js" class="stripe-button"
					data-key="pk_test_JPzQqgx3JRPu0OUwpZy0wFko"
					data-amount="<?php echo $payable_amount; ?>"
					data-name="<?php echo $post->post_title ?>"
					data-description="<?php echo pll__('Resource/Service Backing Payment')?>"
					data-image="http://stage.siriuscrowd.com/wp-content/themes/backer/payment_logo.png"
					data-currency="AUD"
					data-locale="<?php echo $locale ?>"
					data-label="<?php echo pll__("Pay with Stripe") ?>"
					data-panelLabel="<?php echo pll__('Pay')?>">
				</script>
				<input type="hidden" name="payment_amount" value="<?php echo $payable_amount; ?>" />
				<input type="hidden" name="project_id" value="<?php echo $project_id; ?>" />
				<input type="hidden" name="author_id" value="<?php echo $post->post_author; ?>" />
				<input type="hidden" name="post_id" value="<?php echo get_the_ID(); ?>" />
				<input type="hidden" name="payment_description" value="<?php echo "Project id: ".$project_id." - ".$post->post_title ?> - Resource Backing Payment"  />
				</form>
				<br />
				<br />
		<?php	
			    }
			}
			else if($payment_status == 1)
			{
				if($current_user->ID == $post->post_author)
			    {
					echo "<p>". pll__("You've already paid the Resource/Service backing fee to the administrator") . "</p>";
				}
			}
			
			echo "<em>". pll__("Offers for this project, so far")  ."...</em>";
		?>
		
		<!-- Beginning of the HTML section that will show the comment list(Using Backer's styling to maintain the consistency)-->
		<!-- All extra styling have been done inline to keep to minimize the number of files for better re-usability -->
		<ol id="comments-list">
				
		<?php
		//For each offer do the following
		while($row = $offers_result->fetch_assoc())
		{
			$offer_id = $row['resource_id'];
			$user_id = $row['user_id'];
			$user_info = get_userdata($user_id);
			$approved = $row['approved'];
			
			?>
			<li id="comment-<?php comment_ID(); ?>" class="comment clearfix">
			
			<div class="comment-avatar custom-shape hexagon-small"><?php echo get_avatar($user_id); ?></div>

			<div class="comment-content">

				<div class="comment-meta clearfix">

					<h6 class="comment-title"><?php echo pll__('Submitted By'). ": " .$user_info->first_name; ?></h6>
					<span class="comment-date">On: <?php echo $row['offer_date']; ?></span>
					<?php //comment_reply_link( array_merge( $args, array( 'depth' => $depth, 'max_depth' => 3, 'reply_text' =>  '<i class="krown-icon-cw"></i>' . __( 'Reply', 'krown') ) ) ); ?>

				</div>

				<div class="comment-text">

					<?php echo $row['offer']; ?>
					
					<!-- Decide the status line -->
					<?php 
					//Initial selection of status line
					if ($approved == 0) { 
						$status_line = pll__('Your offer is yet to be accepted');
					}else if ($approved == 1) {
						$status_line = pll__('Your offer has been accepted by the project creator');
					} 
					 ?>
					<br/>
					<?php if($current_user->ID == $post->post_author){ ?>
					<div style="display:inline-block; vertical-align: middle; width: 300px;">
					<!-- Check if the user is logged in and is the author of the current post to show the approve button onclick="ajax_submit(<?php //echo $offer_id ?>)" -->
					<?php if($approved == 0) {
							 $btntext = pll__('Approve'); // Initial selection of button text here
					?>
					<button id="aubutton-<?php echo $offer_id ?>" onclick="ajax_submit(<?php echo $offer_id ?>, 'approve');" style="font-family: 'Montserrat';
								   font-weight: 400;
								   color: white;
								   text-align: center;
								   line-height:42px;
								   font-weight: bold;
								   margin-right: 6px;
								   width: 100px;
								   height: 42px;
								   background-image: url('http://stage.siriuscrowd.com/wp-content/themes/backer/bg.png');"><?php echo $btntext; ?></button>
					<?php //$status_line = "Your offer is yet to be accepted" ?>
					
					<?php }
						   else if($approved == 1) {
							 $btntext = pll__('Unapprove'); // Initial selection of button text here
					?>
					
					<button id="aubutton-<?php echo $offer_id ?>" onclick="ajax_submit(<?php echo $offer_id ?>, 'unapprove');" style="font-family: 'Montserrat';
								   font-weight: 400;
								   color: white;
								   text-align: center;
								   line-height:42px;
								   font-weight: bold;
								   margin-right: 6px;
								   width: 100px;
								   height: 42px;
								   background-image: url('http://stage.siriuscrowd.com/wp-content/themes/backer/bg.png');"><?php echo $btntext; ?></button>
					<?php //$status_line = "Your offer has been accepted by the project creator" ?>
				    <?php } ?>
					
					<button onclick="delete_offer(<?php echo $offer_id ?>)" style="font-family: 'Montserrat';
								   font-weight: 400;
								   color: white;
								   text-align: center;
								   line-height:42px;
								   font-weight: bold;
								   margin-right: 6px;
								   width: 88px;
								   height: 42px;
								   background-image: url('http://stage.siriuscrowd.com/wp-content/themes/backer/bg-d.png');"><?php echo pll__('Delete') ?></button>
					
					</div>
					<?php } ?>
					<br />
					<em id="response-<?php echo $offer_id ?>" style="font-size: 75%; color: #177c8b"><?php echo $status_line ?></em>
				</div>

			</div>

			</li>
			<?php
		}
		?>
		</ol>
		<?php
	}
	else
	{
		//If no offers has been made show the following
		echo "<strong>" . pll__('No resource/service offer has been made to this project yet, Be the first one!') ."</strong></br></br>";
	}
	
	
?>

<!-- Allow the offer submission only if the user is logged in --->
<?php if(is_user_logged_in()){?>
		<form method="POST"action="">
		<?php echo pll__('Enter Your Resource/Service Below').":"?>
		<br/>
		<br/>
		<textarea rows="2" cols="10" name="resource"></textarea>
		<br/>
		<input type="submit" value="<?php echo pll__('Submit'); ?>">
		</form>
<?php }
	else
	{
		echo "<strong>". pll__('Please login to back this project with a resource or service!') ."</strong></br></br>";
	}	
?>