/*
	Author: Ravindu Ubeysinghe
	Student ID: 4956567
	Description: The main javascript file (script.js) which contains all the javascript functions used in the site
	
*/

function retrieveLoginInfo() 
{
	var xHRObject = false;

	if (window.XMLHttpRequest)
		xHRObject = new XMLHttpRequest();
	else if (window.ActiveXObject)
		xHRObject = new ActiveXObject("Microsoft.XMLHTTP");
		
	var username = document.getElementById("username").value;
	var password = document.getElementsById("password").value;
      xHRObject.open("GET", "retrieveHotelInfo.php?username=" + username + "password=" + password, true);
      xHRObject.onreadystatechange = function() {
           if (xHRObject.readyState == 4 && xHRObject.status == 200)
               document.getElementById('information').innerHTML = xHRObject.responseText;
      }
      xHRObject.send(null); 
}

function regValidation()
{
    var firstname = document.getElementById("firstname").value;
    var lastname = document.getElementById("lastname").value;
    var email = document.getElementById("email").value;
    var pass = document.getElementById("pass").value;
    var pass2 = document.getElementById("pass2").value;
    var phone = document.getElementById("phone").value;
    var address = document.getElementById("address").value;
	var errormsg = "";
    if(firstname == "" || lastname == "" || email == "" || pass == "" || pass2 == "" || address == ""){
        errormsg += "\nPlease enter all the required details (Phone number excepted).";
    }

    if(pass.value != pass2.value){
        errormsg += "\nPasswords mismatch please try again";
    }
	
	var pattern = new RegExp(/^([0]{1})([\d]{1})([\s]{1})([0-9]{8})*$/);
	
	if(!pattern.test(phone)){
		errormsg += "\nThe phone number that you entered is invalid, it should follow this format 0d dddddddd";
	}
	
	if(errormsg != ""){
		alert(errormsg);
	}else{
		
		var xHRObject = false;
	
		if (window.XMLHttpRequest)
			xHRObject = new XMLHttpRequest();
		else if (window.ActiveXObject)
			xHRObject = new ActiveXObject("Microsoft.XMLHTTP");
	
		xHRObject.open("GET", "register.php?firstname=" + firstname +"&lastname=" + lastname + "&email=" + email + "&pass=" + pass +"&phone="+ phone + "&address=" + address, true);
		xHRObject.onreadystatechange = function() {
	
		if (xHRObject.readyState == 4 && xHRObject.status == 200)
		document.getElementById('information').innerHTML = xHRObject.responseText;
		}
		xHRObject.send(null);
	}
  

}

function mgrValidation()
{
    var mgrid = document.getElementById("mgrid").value;
    var mgrpass = document.getElementById("mgrpass").value;

	var errormsg = "";
    if(mgrid == ""  || mgrpass == ""){
        errormsg += "Please enter all the required details.";
    }
	
	if(errormsg != ""){
		alert(errormsg);
	}else{
		
		var xHRObject = false;
	
		if (window.XMLHttpRequest)
			xHRObject = new XMLHttpRequest();
		else if (window.ActiveXObject)
			xHRObject = new ActiveXObject("Microsoft.XMLHTTP");
	
		xHRObject.open("GET", "mlogin.php?mgrid=" + mgrid +"&mgrpass=" + mgrpass, true);
		xHRObject.onreadystatechange = function() {
	
		if (xHRObject.readyState == 4 && xHRObject.status == 200)
			document.getElementById('information').innerHTML = xHRObject.responseText;
		}
		xHRObject.send(null);
	}
  

}

function logValidation()
{
    var email = document.getElementById("email").value;
    var password = document.getElementById("password").value;

	
	
	var xHRObject = false;
	
	if (window.XMLHttpRequest)
		xHRObject = new XMLHttpRequest();
	else if (window.ActiveXObject)
		xHRObject = new ActiveXObject("Microsoft.XMLHTTP");
	
	xHRObject.open("GET", "login.php?email=" + email +"&password=" + password, true);
	xHRObject.onreadystatechange = function() {
	
	if (xHRObject.readyState == 4 && xHRObject.status == 200)
		var response = xHRObject.responseText;
		if (response == "Login Failed, Please enter correct email and password"){
			document.getElementById('information').innerHTML = xHRObject.responseText;

		}else{
			document.getElementById('information').innerHTML = xHRObject.responseText;
			setTimeout(function () {
       			window.location.href = "buying.htm";}, 3000);
		}
	}
	xHRObject.send(null);
  

}

function mlogout()
{	
	if (window.XMLHttpRequest)
		xHRObject = new XMLHttpRequest();
	else if (window.ActiveXObject)
		xHRObject = new ActiveXObject("Microsoft.XMLHTTP");
	
	xHRObject.open("GET", "mlogout.php", true);
	xHRObject.onreadystatechange = function() {
	
	if (xHRObject.readyState == 4 && xHRObject.status == 200)
		document.getElementById('information').innerHTML = xHRObject.responseText;
	}
	xHRObject.send(null);
  

}

function clogout()
{	
	if (window.XMLHttpRequest)
		xHRObject = new XMLHttpRequest();
	else if (window.ActiveXObject)
		xHRObject = new ActiveXObject("Microsoft.XMLHTTP");
	
	xHRObject.open("GET", "clogout.php", true);
	xHRObject.onreadystatechange = function() {
	
	if (xHRObject.readyState == 4 && xHRObject.status == 200)
		cancelPurchase();
		document.getElementById('ninformation').innerHTML = xHRObject.responseText;
	}
	xHRObject.send(null);
  

}

function buying()
{	
	//alert("sending");
	setInterval(function() {
	//alert("sending");
	if (window.XMLHttpRequest)
		xHRObject = new XMLHttpRequest();
	else if (window.ActiveXObject)
		xHRObject = new ActiveXObject("Microsoft.XMLHTTP");
	
	xHRObject.open("GET", "buying.php", true);
	xHRObject.onreadystatechange = function() {
	
	if (xHRObject.readyState == 4 && xHRObject.status == 200)
		document.getElementById('information').innerHTML = xHRObject.responseText;
	}
	xHRObject.send(null);
	}, 2000);
  

}

function addToCart(itemid, action)
{	
		
		var xHRObject = false;
	
		if (window.XMLHttpRequest)
			xHRObject = new XMLHttpRequest();
		else if (window.ActiveXObject)
			xHRObject = new ActiveXObject("Microsoft.XMLHTTP");
	
		xHRObject.open("GET", "addtocart.php?itemid=" + itemid + "&action=" + action, true);
		xHRObject.onreadystatechange = function() {
	
		if (xHRObject.readyState == 4 && xHRObject.status == 200)
			document.getElementById('information').innerHTML = document.getElementById('information').innerHTML + xHRObject.responseText;
		}
		xHRObject.send(null);

}

function mProcess(){
		setInterval(function() {
		//
		var xHRObject = false;
	
		if (window.XMLHttpRequest)
			xHRObject = new XMLHttpRequest();
		else if (window.ActiveXObject)
			xHRObject = new ActiveXObject("Microsoft.XMLHTTP");
	
		xHRObject.open("GET", "mProcess.php", true);
		xHRObject.onreadystatechange = function() {
	
		if (xHRObject.readyState == 4 && xHRObject.status == 200)
			document.getElementById('final').innerHTML = xHRObject.responseText;
		}
		xHRObject.send(null);
		}, 2000);
}

function process()
{
	   if (window.ActiveXObject)
	   {
           xHRObject = new ActiveXObject("Microsoft.XMLHTTP");
           }
           else if (window.XMLHttpRequest)
           {
           xHRObject = new XMLHttpRequest();
           }

           xHRObject.open("GET", "processing.php", true);
           //xHRObject.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
           xHRObject.onreadystatechange = function(){
		if ((xHRObject.readyState == 4) &&(xHRObject.status == 200))
	{
		if (window.ActiveXObject)
		{
			//Load XML
			var xml = xHRObject.responseXML;
		
			//Load XSL
			var xsl = new ActiveXObject("Microsoft.XMLDOM");
			xsl.async = false;
			xsl.load("processing.xsl");
			
			//Transform
			var transform = xml.transformNode(xsl);
			var container = document.getElementById("information");
			container.innerHTML = transform; 
		}
		else
		{
			var xsltProcessor = new XSLTProcessor();
			
			//Load XSL
			xslStylesheet = document.implementation.createDocument("", "doc", null);
			xslStylesheet.async = false;
			xslStylesheet.load("processing.xsl"); 
			xsltProcessor.importStylesheet(xslStylesheet);
			//Load XML
			xmlDoc = xHRObject.responseXML;
			
			//Transform
			var fragment = xsltProcessor.transformToFragment(xmlDoc, document);
			document.getElementById("information").innerHTML = new XMLSerializer().serializeToString(fragment);
		}
	}

	   }
           xHRObject.send(); 
}

function confirmPurchase()
{	
		
		var xHRObject = false;
	
		if (window.XMLHttpRequest)
			xHRObject = new XMLHttpRequest();
		else if (window.ActiveXObject)
			xHRObject = new ActiveXObject("Microsoft.XMLHTTP");
	
		xHRObject.open("GET", "confirmPurchase.php", true);
		xHRObject.onreadystatechange = function() {
	
		if (xHRObject.readyState == 4 && xHRObject.status == 200)
			document.getElementById('final').innerHTML = xHRObject.responseText;
			setTimeout(function () {
       			window.location.href = "clogout.htm";}, 5000);

		}
		xHRObject.send(null);

}

function cancelPurchase()
{	
		//
		var xHRObject = false;
	
		if (window.XMLHttpRequest)
			xHRObject = new XMLHttpRequest();
		else if (window.ActiveXObject)
			xHRObject = new ActiveXObject("Microsoft.XMLHTTP");
	
		xHRObject.open("GET", "cancelPurchase.php", true);
		xHRObject.onreadystatechange = function() {
	
		if (xHRObject.readyState == 4 && xHRObject.status == 200)
			document.getElementById('final').innerHTML = xHRObject.responseText;
		}
		xHRObject.send(null);

}

function itemValidation()
{
    var itemName = document.getElementById("itemName").value;
    var itemPrice = document.getElementById("itemPrice").value;
	var itemQty = document.getElementById("itemQty").value;
	var itemDes = document.getElementById("itemDes").value;

	var errormsg = "";
    if(itemName == ""  || itemPrice == "" || itemQty == "" || itemDes == ""){
        errormsg += "Please enter all the required details.";
    }
	
	if(!isNaN(itemName)){
		errormsg += "\nPlease enter only alphabetical characters for name";
	}
	
	if(isNaN(itemPrice)){
		errormsg += "\nPlease enter only numeric characters for price";
	}
	
	if(isNaN(itemQty)){
		errormsg += "\nPlease enter only numeric characters for quantity";
	}
	
	
	if(errormsg != ""){
		alert(errormsg);
	}else{
		
		var xHRObject = false;
	
		if (window.XMLHttpRequest)
			xHRObject = new XMLHttpRequest();
		else if (window.ActiveXObject)
			xHRObject = new ActiveXObject("Microsoft.XMLHTTP");
	
		xHRObject.open("GET", "listing.php?name=" + itemName +"&price=" + itemPrice + "&qty=" + itemQty + "&des=" + itemDes, true);
		xHRObject.onreadystatechange = function() {
	
		if (xHRObject.readyState == 4 && xHRObject.status == 200)
			document.getElementById('information').innerHTML = xHRObject.responseText;
		}
		xHRObject.send(null);
	}
  

}

function resetItem(){
	document.getElementById("itemName").value = "";
    	document.getElementById("itemPrice").value = "";
	document.getElementById("itemQty").value = "";
	document.getElementById("itemDes").value = "";
}