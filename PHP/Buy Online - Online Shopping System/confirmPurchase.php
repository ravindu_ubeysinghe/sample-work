<!--
	Author: Ravindu Ubeysinghe
	Student ID: 4956567
	Description: The PHP script which let's the customer confirm all the items in the shopping cart and proceed to checkout
-->

<?php
 session_register('Cart');
 $HTML = "";
 $xmlFile = "/home/students/accounts/s4956567/cos30020/www/data/goods.xml";
 $total = 0;
 $matches = false;
 if(isset($_SESSION['Cart']))
 {
	$MDA = $_SESSION['Cart'];
	
	foreach($MDA as $value)
	{	
		if($value['qty'] > 0 ){
			
			$xml = DOMDocument::load($xmlFile);
			$item = $xml->getElementsByTagName("Item");
			
			foreach ($item as $key=>$node) {
			$itemid_output = $node->getElementsByTagName("Itemid");
			$itemid_output = $itemid_output->item(0)->nodeValue;
		
				if($itemid_output == $value['itemno']){
					$total = $total + $value['price'];
					$qtyhold_output = $node->getElementsByTagName("QtyOnHold");
					$qtyhold_output = $qtyhold_output->item(0)->nodeValue;

					$xml->getElementsByTagName("QtyOnHold")->item($key)->nodeValue-=$qtyhold_output;
					$xml->save($xmlFile);
					$xml->getElementsByTagName("QtyOnSold")->item($key)->nodeValue+=$qtyhold_output;
					$xml->save($xmlFile);
					$matches = true;
				}
			}
		}
	}
	if($matches== true){
		unset($_SESSION['Cart']);
		$HTML = $HTML. "YOUR PURCHASE HAS BEEN CONFIRMED, AND THE TOTAL AMOUNT DUE TO PAY IS, $" . $total;
	}
 }
 else{
	$HTML = $HTML. "Please add some items to the cart first";
 }
ECHO $HTML;

?>