<!--
	Author: Ravindu Ubeysinghe
	Student ID: 4956567
	Description: The PHP script which let's the customer log out from the system	
-->

<?php
 session_start();
 if(isset($_SESSION["customerid"])){
	if(isset($_SESSION["Cart"])){
		$HTML = "<script> 'cancelPurchase()' </script><br />";
		$HTML = $HTML . "<p>Thank you for using the system ". $_SESSION["customerid"] .", See you next time!</p>";
	}else{
		$HTML = "Thank you for using the system ". $_SESSION["customerid"] .", See you next time!";

	}
 }else{
	$HTML = "Please login first ";
 }

 session_destroy();
ECHO $HTML;

?>