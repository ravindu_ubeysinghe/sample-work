<!--
	Author: Ravindu Ubeysinghe
	Student ID: 4956567
	Description: The PHP script which generates the product catalog for the users with all the available items	
-->

<?php
 session_start();
 $xmlFile = "/home/students/accounts/s4956567/cos30020/www/data/goods.xml";
 $itemid = "1";
 $HTML = "";
 if(isset($_SESSION['customerid'])){
	$HTML = "<a href = \" clogout.htm\"><p>Logout</p></a><hr />";
 

	if(file_exists($xmlFile))
	{
		$xml = DOMDocument::load($xmlFile);
		$item = $xml->getElementsByTagName("Item");
	
		$count = 0;
		
		$HTML = $HTML. "<table border=\"1\">
						<tr>
							<th>Item Number</th>
							<th>Name</th> 
							<th>Description</th>
							<th>Price</th>
							<th>Quantity</th>
							<th>Add</th>
						</tr>
						";
		
		foreach ($item as $node) {
			$itemid_output = $node->getElementsByTagName("Itemid");
			$itemid_output = $itemid_output->item(0)->nodeValue;
		
			$name_output = $node->getElementsByTagName("Name");
			$name_output = $name_output->item(0)->nodeValue;
			
			$price_output = $node->getElementsByTagName("Price");
			$price_output = $price_output->item(0)->nodeValue;
		
			$des_output = $node->getElementsByTagname("Des");
			$des_output = $des_output->item(0)->nodeValue;
			
			$qty_output = $node->getElementsByTagname("Quantity");
			$qty_output = $qty_output->item(0)->nodeValue;
			
			$HTML = $HTML. "
						<tr>
							<td> ". $itemid_output . "</td>
							<td> ". $name_output . "</td> 
							<td> ". substr($des_output, 0, 20) . "</td>
							<td> ". $price_output ."</td>
							<td> ". $qty_output . "</td>
							<td>  <button onclick='addToCart(\"" . $itemid_output . "\", \"add\")';>Add one to cart</button> </td>
						</tr>
						";
		}
		$HTML = $HTML. "</table>";
		$HTML = $HTML. "<h1> Shopping Cart </h1> <br />";
		if(!(isset($_SESSION["Cart"]))){
			$HTML = $HTML. "<table border=\"1\">
					<tr>
					<th>Item Number</th>
					<th>Price</th>
					<th>Quantity</th>
					<th>Remove</th>
					</tr>
					<tr>
					<td colspan = \"4\">Your shopping cart is empty</td>
					</tr>
					</table>";
		}else{
		
			if((sizeof($_SESSION['Cart'])) != 0){
				$total = 0;
				$MDA = $_SESSION["Cart"];
				$HTML = $HTML. "<table border=\"1\">
					<tr>
					<th>Item Number</th>
					<th>Price</th>
					<th>Quantity</th>
					<th>Remove</th>
					</tr>";
				foreach($MDA as $value)
				{	
					if($value['qty'] > 0){
					$total = $total + $value['price'];
					$HTML = $HTML. 
					"<tr>
					<td>". $value['itemno'] ."</td>
					<td>". $value['price'] ."</td>
					<td>". $value['qty'] ."</td>
					<td> <button onclick='addToCart(\"" . $value['itemno'] . "\", \"remove\")';>Remove from cart</button></td>
					</tr>";
					}
				}
				$HTML = $HTML. "<tr>
					<td></td>
					<td></td>
					<td>Total</td>
					<td>$" . $total. "</td>
					</tr>
					<tr>
					<td colspan=\"2\"><button onclick='confirmPurchase()';>Confirm Purchase</button></td>
					<td colspan=\"2\"><button onclick='cancelPurchase()';>Cancel Purchase</button></td>
					</tr>";
				$HTML = $HTML. "</table>";
			}else{
					$HTML = $HTML. "<table border=\"1\">
					<tr>
					<th>Item Number</th>
					<th>Price</th>
					<th>Quantity</th>
					<th>Remove</th>
					</tr>
					<tr>
					<td colspan = \"4\">Your shopping cart is empty</td>
					</tr>
					</table>";
			}
		}
	}
	else{
		$HTML = $HTML. "No items exists in the system at the moment, please try again later!";
	}
}
else{
	$HTML = $HTML. "Please login first!";
}

ECHO $HTML;

?>