<!--
	Auhor: Ravindu Ubeysinghe
	Student ID: 4956567
	Page Description: The XLS style sheet which gets all the items available and show them in HTML 
-->

<?xml version="1.0"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                version="1.0">
 
    <xsl:output method="html"/>

    <xsl:template match="/">
	<a href="listing.php"><p style="display: inline; padding-right: 10px;">Listing</p></a><a href="processing.htm"><p style="display: inline; padding-right: 10px;">Processing</p></a>
	<a href="mlogout.htm"><p style="display: inline;">Log out</p></a>
	<br />
	<hr />
	<TABLE border="1">
	<TR>
		<TH>Item Number </TH>
		<TH>Item Name </TH>
		<TH>Price </TH>
		<TH>Quantity Available </TH>
		<TH>Quantity On Hold </TH>
		<TH>Quantity Sold </TH>
	</TR>
            <xsl:for-each select="/Items/Item">
		<xsl:if test="QtyOnSold>0">
                <TR>
                    <TD><xsl:value-of select="Itemid"/></TD>
                    <TD><xsl:value-of select="Name"/></TD>
		    <TD><xsl:value-of select="Price"/></TD>
		    <TD><xsl:value-of select="Quantity"/></TD>
		    <TD><xsl:value-of select="QtyOnHold"/></TD>
		    <TD><xsl:value-of select="QtyOnSold"/></TD>
                </TR>
		</xsl:if>
	     </xsl:for-each>
	<TR>
		<TD colspan="6" align="center"> <button onclick="mProcess()">Process</button></TD>
	</TR>
	</TABLE>
    </xsl:template>

</xsl:stylesheet>
 