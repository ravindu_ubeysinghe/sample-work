<!--
	Author: Ravindu Ubeysinghe
	Student ID: 4956567
	Description: The PHP script which allows the customers to cancel all of their items in the shopping cart and logout	
-->

<?php
 session_register('Cart');
 $HTML = "";
 $xmlFile = "/home/students/accounts/s4956567/cos30020/www/data/goods.xml";
 $tempVar = "";
 $itemid = $_GET["itemid"];
 $action = $_GET["action"];
 if(file_exists($xmlFile))
 {
	$xml = DOMDocument::load($xmlFile);
	$item = $xml->getElementsByTagName("Item");

	$count = 0;
	
	foreach ($item as $node) {
		$itemid_output = $node->getElementsByTagName("Itemid");
		$itemid_output = $itemid_output->item(0)->nodeValue;
	
		if($itemid == $itemid_output){
			$qty_output = $node->getElementsByTagName("Quantity");
			$qty_output = $qty_output->item(0)->nodeValue;
			
			if($qty_output > 0){
				if($action == "add"){
					$xml->getElementsByTagName("Quantity")->item(0)->nodeValue--;
					$xml->save($xmlFile);
					$xml->getElementsByTagName("QtyOnHold")->item(0)->nodeValue++;
					$xml->save($xmlFile);
				}else{
					$xml->getElementsByTagName("Quantity")->item(0)->nodeValue++;
					$xml->save($xmlFile);
					$xml->getElementsByTagName("QtyOnHold")->item(0)->nodeValue--;
					$xml->save($xmlFile);
				}

				$price_output = $node->getElementsByTagName("Price");
				$price_output = $price_output->item(0)->nodeValue;

				$qtyonhold_output = $node->getElementsByTagName("QtyOnHold");
				$qtyonhold_output = $qtyonhold_output->item(0)->nodeValue;

				if($_SESSION["Cart"] == ""){
					$data = array("itemno" => $itemid, "price" => $price_output, "qty" => 1);
					$MDA = array($itemid => $data);

				}
				else{ 
					$MDA = $_SESSION["Cart"];
					if(!(array_key_exists($itemid, $MDA))){
						//$HTML = $HTML. "doesnt Exists";
						$data = array("itemno" => $itemid, "price" => $price_output, "qty" => 1);
						$MDA = $MDA + array($itemid => $data);
					}else{
						if($action == "add"){
							$MDA[$itemid]['qty']++;
						}elseif($action == "remove"){
							$MDA[$itemid]['qty']--;
						}
					}
				}
				$_SESSION["Cart"] = $MDA;
			}
			else{
				$HTML = $HTML."Sorry, this item is not available for sale";
			}
		}
	}
}
else{
	$HTML = $HTML. "No items exists in the system at the moment, please try again later!";
}

ECHO $HTML;

?>