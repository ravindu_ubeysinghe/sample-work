<!--
	Author: Ravindu Ubeysinghe
	Student ID: 4956567
	Description: The PHP script which let's the customer login to the system
-->

<?php
 session_start();
 $_SESSION["customerid"] = "";
 $xmlFile = "/home/students/accounts/s4956567/cos30020/www/data/customer.xml";
 $HTML = "";
 $count = 0;
 $email = $_GET['email'];
 $password = $_GET['password'];
 $dom = DOMDocument::load($xmlFile);
 $customer = $dom->getElementsByTagName("Customer"); 
 $found = false;
foreach ($customer as $node) {
		$email_output = $node->getElementsByTagName("Email");
		$email_output = $email_output->item(0)->nodeValue;
	
		$pass_output = $node->getElementsByTagName("Pass");
		$pass_output = $pass_output->item(0)->nodeValue;
	
	
		if( ($email == $email_output) && ($password == $pass_output) ){
			$name_output = $node->getElementsByTagName("Firstname");
			$name_output = $name_output->item(0)->nodeValue;

			$_SESSION['customerid'] = $name_output;
			$found = true;
			break;
		}
}

if($found == true){
	$HTML = "You're successfully logged in to the system";
}else{
	$HTML = "Login Failed, Please enter correct email and password";
}

ECHO $HTML;

?>