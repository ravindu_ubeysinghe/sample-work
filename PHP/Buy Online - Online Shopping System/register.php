<!--
	Auhor: Ravindu Ubeysinghe
	Student ID: 4956567
	Page Description: The PHP Script which allows customers to register into the system
-->

<?php

$xmlFile = "/home/students/accounts/s4956567/cos30020/www/data/customer.xml";
$HTML = "";

$firstname = $_GET["firstname"];
$lastname = $_GET["lastname"];
$email = $_GET["email"];
$pass = $_GET["pass"];
$phone = $_GET["phone"];
$address = $_GET["address"];

if(!file_exists($xmlFile))
{
	/*umask(0007);
	$directory = "../Assignment2";
	if(!is_dir($directory))
	{
		mkdir($directory, 0777);
	}

	$f = fopen("customer.xml", "w+");*/
	
	$xml = new DOMDocument('1.0', 'utf-8');
	$xml_customers = $xml->createElement("Customers");
	$xml_customer = $xml->createElement("Customer");
	$xml_customerid = $xml->createElement("Customerid", '1');
	$xml_pass = $xml->createElement("Pass", $pass);
	$xml_firstname = $xml->createElement("Firstname", $firstname);
	$xml_lastname = $xml->createElement("Lastname", $lastname);
	$xml_email = $xml->createElement("Email", $email);
	$xml_address = $xml->createElement("Address", $address);
	
	$xml_customer->appendChild( $xml_customerid );
	$xml_customer->appendChild( $xml_pass );
	$xml_customer->appendChild( $xml_firstname );
	$xml_customer->appendChild( $xml_lastname );
	$xml_customer->appendChild( $xml_email );
	$xml_customer->appendChild( $xml_address );
	
	$xml_customers->appendChild( $xml_customer );
	$xml->appendChild( $xml_customers );
	
	$xml->formatOutput = true;
	$xml->saveXML();
	$xml->save("/home/students/accounts/s4956567/cos30020/www/data/customer.xml");
    	chmod("/home/students/accounts/s4956567/cos30020/www/data/customer.xml", 0777);

	$HTML = "";
	$dom = DOMDocument::load("customer.xml");
	$customer = $dom->getElementsByTagName("Customer");

	$count = 0;
	foreach ($customer as $node) {
		$customerid_output = $node->getElementsByTagName("Customerid");
		$customerid_output = $customerid_output->item(0)->nodeValue;
	
		$firstname_output = $node->getElementsByTagName("Firstname");
		$firstname_output = $firstname_output->item(0)->nodeValue;
	
		$email_output = $node->getElementsByTagname("Email");
		$email_output = $email_output->item(0)->nodeValue;
	
	
		if( ($email_output == $email) ){
			$HTML = "Dear ".$firstname_output."   . You have been successfully registered into the system. Please use your Id ".$customerid_output." To log into the system";
			$count++;
		}
	}
		if($count == 0){
			$HTML = "Data couldn't be inserted into system, please try again later".$email;
	
	
	
		}

		echo $HTML;

}
else
{
	$HTML = "";
	$xml = DOMDocument::load($xmlFile);
	$customer = $xml->getElementsByTagName("Customer");

	$count = 0;
	
	//Checking if the email address already exists
	
	foreach ($customer as $node) {
		$email_output = $node->getElementsByTagName("Email");
		$email_output = $email_output->item(0)->nodeValue;
	
		if ($email_output == $email){
			return exit("The email address already exists in the system, please use a different one or if you're an existing user Please login");
		}
	
	}
	
	foreach ($customer as $node) {
		$customerid_output = $node->getElementsByTagName("Customerid");
		$customerid_output = $customerid_output->item(0)->nodeValue;
	
		$obtainedCustomerid = $customerid_output;
	
	}
	
	$obtainedCustomerid = $obtainedCustomerid + 1;
	
	$xml_customers = $xml->firstChild;

	$xml_customer = $xml->createElement("Customer");
	$xml_customerid = $xml->createElement("Customerid", $obtainedCustomerid);
	$xml_pass = $xml->createElement("Pass", $pass);
	$xml_firstname = $xml->createElement("Firstname", $firstname);
	$xml_lastname = $xml->createElement("Lastname", $lastname);
	$xml_email = $xml->createElement("Email", $email);
	$xml_address = $xml->createElement("Address", $address);
	
	$xml_customer->appendChild( $xml_customerid );
	$xml_customer->appendChild( $xml_pass );
	$xml_customer->appendChild( $xml_firstname );
	$xml_customer->appendChild( $xml_lastname );
	$xml_customer->appendChild( $xml_email );
	$xml_customer->appendChild( $xml_address );
	
	$xml_customers->appendChild( $xml_customer );
	
	$xml->formatOutput = true;
	$xml->saveXML();
	$xml->save("/home/students/accounts/s4956567/cos30020/www/data/customer.xml");
    	chmod("/home/students/accounts/s4956567/cos30020/www/data/customer.xml", 0777);
	
	$customer = $xml->getElementsByTagName("Customer");
	
	$count = 0;
	foreach ($customer as $node) {
		$customerid_output = $node->getElementsByTagName("Customerid");
		$customerid_output = $customerid_output->item(0)->nodeValue;
	
		$firstname_output = $node->getElementsByTagName("Firstname");
		$firstname_output = $firstname_output->item(0)->nodeValue;
	
		$email_output = $node->getElementsByTagname("Email");
		$email_output = $email_output->item(0)->nodeValue;
	
			if( ($email_output == $email) ){
				$HTML = "Dear ".$firstname_output."   . You have been successfully registered into the system. Please use your Id ".$customerid_output." To log into the system";
				$count++;
			}
		}
		if($count == 0){
			$HTML = "Data couldn't be inserted into system, please try again later 2".$email;
	
	
	
		}
		echo $HTML;

}

?>