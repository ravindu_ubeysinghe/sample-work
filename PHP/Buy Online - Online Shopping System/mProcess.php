<!--
	Author: Ravindu Ubeysinghe
	Student ID: 4956567
	Description: The PHP script which let's the managers process the sold items list by getting rid of the sold itmes and the items without any quantity 
-->

<?php
 $HTML = "";
 $total = 0;
 $xmlFile = "/home/students/accounts/s4956567/cos30020/www/data/goods.xml";
 $index;
 $matches = false;
 $xml = DOMDocument::load($xmlFile);
 $items = $xml->documentElement;
 $item = $xml->getElementsByTagName("Item");
			
	foreach ($item as $key=>$node) {
			$qtysold_output = $node->getElementsByTagName("QtyOnSold");
			$qtysold_output = $qtysold_output->item(0)->nodeValue;
			
			$qty_output = $node->getElementsByTagName("Quantity");
			$qty_output = $qty_output->item(0)->nodeValue;

			if($qtysold_output > 0){
				$xml->getElementsByTagName("QtyOnSold")->item($key)->nodeValue-=$qtysold_output;
				$xml->save($xmlFile);
				//$HTML = "setted";
			}
			
			if($qty_output < 1){
				$deleteItem = $xml->getElementsByTagName("Item")->item($key);
				$deleteItem->parentNode->removeChild($deleteItem);
				$xml->save($xmlFile);

			}
	}
	
	
ECHO $HTML;

?>