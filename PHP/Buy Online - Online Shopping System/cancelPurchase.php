<!--
	Author: Ravindu Ubeysinghe
	Student ID: 4956567
	Description: The PHP script which allows the customers to cancel all of their items in the shopping cart and logout	
-->

<?php
 session_register('Cart');
 $HTML = "";
 $xmlFile = "/home/students/accounts/s4956567/cos30020/www/data/goods.xml";
 $total = 0;
 $matches = false;
 if(sizeof($_SESSION['Cart']) != 0)
 {
	$MDA = $_SESSION['Cart'];
	
	foreach($MDA as $value)
	{	
		if($value['qty'] > 0 ){
			
			$xml = DOMDocument::load($xmlFile);
			$item = $xml->getElementsByTagName("Item");
			
			foreach ($item as $key=>$node) {
			$itemid_output = $node->getElementsByTagName("Itemid");
			$itemid_output = $itemid_output->item(0)->nodeValue;
		
				if($itemid_output == $value['itemno']){
					$qtyhold_output = $node->getElementsByTagName("QtyOnHold");
					$qtyhold_output = $qtyhold_output->item(0)->nodeValue;

					$xml->getElementsByTagName("QtyOnHold")->item($key)->nodeValue-=$qtyhold_output;
					$xml->save($xmlFile);
					$xml->getElementsByTagName("Quantity")->item($key)->nodeValue+=$qtyhold_output;
					$xml->save($xmlFile);
					$matches = true;
				}
			}
		}
	}
	if($matches== true){
		unset($_SESSION['Cart']);
		$HTML = $HTML. "YOUR PURCHASE REQUEST HAS BEEN CANCELLED, WELCOME TO THE SHOP NEXT TIME!";
	}
 }
 else{
	$HTML = $HTML. "Your cart has been emptied successfully";
 }
ECHO $HTML;

?>