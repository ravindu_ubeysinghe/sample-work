<!--
	Author: Ravindu Ubeysinghe
	Student ID: 4956567
	Description: The PHP script which let's the managers logout from the system
-->

<?php
 session_start();
 if(isset($_SESSION["sid"])){
	$HTML = "Thank you for using the system ". $_SESSION["sid"] .", See you next time!";
 }else{
	$HTML = "Please login first ";
 }

 session_destroy();
ECHO $HTML;

?>