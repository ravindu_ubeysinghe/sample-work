<!--
	Author: Ravindu Ubeysinghe
	Student ID: 4956567
	Description: The PHP script which generates the product catalog from all the available items in the system
-->

<?php
$xmlFile = "/home/students/accounts/s4956567/cos30020/www/data/goods.xml";
$HTML = "";

$name = $_GET["name"];
$price = $_GET["price"];
$qty = $_GET["qty"];
$des = $_GET["des"];
$qtyOnHold = 0;
$qtyOnSold = 0;
$itemid = 1;

if(!file_exists($xmlFile))
{
	/*umask(0007);
	$directory = "../Assignment2";
	if(!is_dir($directory))
	{
		mkdir($directory, 0777);
	}

	$f = fopen("goods.xml", "w+");*/
	
	$xml = new DOMDocument('1.0', 'utf-8');
	//$xslt = $xml->createProcessingInstruction('xml-stylesheet', 'type="text/xsl" version="2.0" href="base.xsl"');
	//$xml->appendChild($xslt);
	$xml_items = $xml->createElement("Items");
	$xml_item = $xml->createElement("Item");
	$xml_itemid= $xml->createElement("Itemid", $itemid);
	$xml_name= $xml->createElement("Name", $name);
	$xml_price = $xml->createElement("Price", $price);
	$xml_qty = $xml->createElement("Quantity", $qty);
	$xml_des = $xml->createElement("Des", $des);
	$xml_qtyonhold = $xml->createElement("QtyOnHold", $qtyOnHold);
	$xml_qtyonsold = $xml->createElement("QtyOnSold", $qtyOnSold);
	
	$xml_item->appendChild( $xml_itemid );
	$xml_item->appendChild( $xml_name );
	$xml_item->appendChild( $xml_price );
	$xml_item->appendChild( $xml_qty);
	$xml_item->appendChild( $xml_des );
	$xml_item->appendChild( $xml_qtyonhold );
	$xml_item->appendChild( $xml_qtyonsold );
	
	$xml_items->appendChild( $xml_item );
	$xml->appendChild( $xml_items );
	
	$xml->formatOutput = true;
	$xml->saveXML();
	$xml->save("/home/students/accounts/s4956567/cos30020/www/data/goods.xml");
    	chmod("/home/students/accounts/s4956567/cos30020/www/data/goods.xml", 0777);

	$HTML = "";
	$dom = DOMDocument::load("goods.xml");
	$item = $dom->getElementsByTagName("Item");

	$count = 0;
	foreach ($item as $node) {
		$itemid_output = $node->getElementsByTagName("Itemid");
		$itemid_output = $itemid_output->item(0)->nodeValue;
	
		$name_output = $node->getElementsByTagName("Name");
		$name_output = $name_output->item(0)->nodeValue;
	
		$des_output = $node->getElementsByTagname("Des");
		$des_output = $des_output->item(0)->nodeValue;
	}
		$HTML = "The item ".$itemid_output."   . has been successfully registered into the system";
		echo $HTML;

}
else
{
	$HTML = "";
	$xml = DOMDocument::load($xmlFile);
	$item = $xml->getElementsByTagName("Item");

	$count = 0;
	
	//Checking if the email address already exists
	
	foreach ($item as $node) {
		$itemid_output = $node->getElementsByTagName("Itemid");
		$itemid_output = $itemid_output->item(0)->nodeValue;
	
		$nextId = $itemid_output;
	
	}
	
	$nextId = $nextId + 1;
	
	$xml_items = $xml->firstChild;

	$xml_item = $xml->createElement("Item");
	$xml_itemid= $xml->createElement("Itemid", $nextId);
	$xml_name= $xml->createElement("Name", $name);
	$xml_price = $xml->createElement("Price", $price);
	$xml_qty = $xml->createElement("Quantity", $qty);
	$xml_des = $xml->createElement("Des", $des);
	$xml_qtyonhold = $xml->createElement("QtyOnHold", $qtyOnHold);
	$xml_qtyonsold = $xml->createElement("QtyOnSold", $qtyOnSold);
	
	$xml_item->appendChild( $xml_itemid );
	$xml_item->appendChild( $xml_name );
	$xml_item->appendChild( $xml_price );
	$xml_item->appendChild( $xml_qty );
	$xml_item->appendChild( $xml_des );
	$xml_item->appendChild( $xml_qtyonhold );
	$xml_item->appendChild( $xml_qtyonsold );
	
	$xml_items->appendChild( $xml_item );
	
	$xml->formatOutput = true;
	$xml->saveXML();
	$xml->save("/home/students/accounts/s4956567/cos30020/www/data/goods.xml");
    	chmod("/home/students/accounts/s4956567/cos30020/www/data/goods.xml", 0777);
	
	$item = $xml->getElementsByTagName("Item");

	$count = 0;
	foreach ($item as $node) {
		$itemid_output = $node->getElementsByTagName("Itemid");
		$itemid_output = $itemid_output->item(0)->nodeValue;
	
		$name_output = $node->getElementsByTagName("Name");
		$name_output = $name_output->item(0)->nodeValue;
	
		$des_output = $node->getElementsByTagname("Des");
		$des_output = $des_output->item(0)->nodeValue;
	}
		$HTML = "The item ".$itemid_output."   . has been successfully registered into the system";
		echo $HTML;
}

?>