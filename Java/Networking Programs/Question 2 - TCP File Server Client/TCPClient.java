import java.io.*; // Import java exception class
import java.net.*; // Import java IO exception class
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*; // Import java networking packag

/**
 * @author Ravindu, 4956567
 * This class is the client class which send the TCPServer a request for a file, receives bytes and construct a new file with the received bytes
 */

public class TCPClient {
	
	/**System.out.println("Writing the file");
	 * 
	 * @param args - command line arguments
	 * @throws IOException
	 * @throws InterruptedException
	 */
	public static void main(String [] args) throws IOException, InterruptedException
	{
		//Declaring local variables for the request, request splitted array, the ip address, port and the byte array
		String filename = "", ipAddress = "";
		int port = 0;
		byte[] input = new byte[1024];
		byte[] filestatus = new byte[1];
		
		//Checking if the port no, ip and filename have been entered as a command line arguments
    	if(args.length == 3){
    		//Assigning the argument one to the ip address
    		ipAddress = args[0];
    		
    		try{
    			//Checking if the port no is valid
    			port = Integer.parseInt(args[1]);
    		}catch(Exception e){
    			System.out.println(e.getMessage());
            	Thread.sleep(5000);
            	System.exit(0);
    		}
            
            if(port < 1024 && port > 65535){
            	System.out.println("You've used a well known port or an invalid port, system exiting!");
            	Thread.sleep(5000);
            	System.exit(0);
            }
            
            //Obtaining the argument number 3 and assigning it to the filename 
            filename = args[2];
    	}
    	else
    	{
    		System.out.println("You should provide a command line argument containing the server port number, system exiting!");
    		Thread.sleep(5000);
    		System.exit(0);
    	}
		
		//Create a new socket with the given ip address and the port 
		Socket jSocket = new Socket(ipAddress, port);
		
		//Obtain the outgoing socket's output stream and include it in a print writer
		PrintWriter dos = new PrintWriter(jSocket.getOutputStream(), true);
	
		//Send the request using the print writer
		dos.println("Send " + filename);
		
		//Feed any bytes in the buffer to the output stream
		dos.flush();
		
		//Obtain the input stream from the socket
		DataInputStream dis = new DataInputStream(jSocket.getInputStream());
		
		//Read the first byte from the input stream of the socket
		dis.read(filestatus, 0, 1);
				
		//Construct a string from the first byte
		String filestatusin = new String(filestatus);
		
		//Checking the file status, "t" for found, "f" for not found
		if(filestatusin.equals("t"))
		{			
			String clientFilename = Paths.get(filename).getFileName().toString();
			
			//Opens a new file in the working directory given by the user
			File outputFile = new File(clientFilename);
		
			//Create a new buffered output stream and include the file in it so that it can be used to write to the file
			BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(outputFile));
		
			//Declaring a new variable to store the number of bytes read
			int byteCount = 0;
			
			try{
				System.out.println("Downloading...");
				//Write to the file until the input stream runs out of bytes
				while((byteCount = dis.read(input)) != -1)
				{
					bos.write(input, 0, byteCount);
		
					bos.flush();
				}
				
				System.out.println("File successfully obtained");
			}catch(IOException e){
				System.out.println("The server went offline unexpectedly");
			}
		}
		else
		{
			//If fielstatus isn't "t" means file not found
			System.out.println("File Not Found");		
		}
		
		//Close the socket
		jSocket.close();
	}
}