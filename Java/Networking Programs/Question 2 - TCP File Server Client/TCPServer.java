import java.io.*; // Import java exception class
import java.io.IOException; // Import java IO exception class
import java.net.*; // Import java networking package

/**
 * @author Ravindu, 4956567
 * This is the class which handles the requests from the TCPClient
 * This receives a request from the TCPClient, obtains the request file, convert it to bytes and sends to TCPClient to be reconstructed
 */

	

public class TCPServer {
	/**
	 * 
	 * @param args - command line arguments
	 * @throws IOException
	 */
	public static void main(String [] args) throws IOException
	{
		ServerSocket tcpSocket = new ServerSocket(4002); //Opens a new TCP socket for the server to listen to new connections
		String filestatus;
		while(true)
		{
			//Set the filestatus to "n" by default so if the file is found it will be changed or it won't
			filestatus = "n";
			
			System.out.println("\nServer waiting for a new connection"); 
			
			//Sever socket waits for a connection and as soon as a connection is established returns a socket
			Socket jSocket = tcpSocket.accept();
			
			System.out.println("Client connected");
			
			//Using a buffered reader to read the input stream from the incoming socket (a more efficient way of reading)
			BufferedReader br = new BufferedReader(new InputStreamReader(jSocket.getInputStream()));
			
			//Get the request line from the client split it based on a space character and get the second portion which is the filename
			String[] request = br.readLine().split("\\s+");
			
			//Try to open the file with the obtained file name, if the file doesn't exist throw an exception
			if(new File(request[1]).exists())
			{
				//Set the file status to "t" because at this stage the file is found
				filestatus = "t";
				
				//Open the file
				File fileToBeSent = new File(request[1]);
				
				System.out.println("File opened");
				
				//Create a new byte array with the size of the length of the file
				byte[] output = new byte[(int) fileToBeSent.length()];
				
				//Create a new buffered input stream to read from the file
				BufferedInputStream fbr = new BufferedInputStream(new FileInputStream(fileToBeSent));
				
				//Read from the file using the buffered input stream and store each and every byte in the byte array created
				fbr.read(output, 0, output.length); 
				
				System.out.println("Reading the file");
				
				//Obtain the socket's output stream and put it in a buffered output stream
				BufferedOutputStream outputToClient = new BufferedOutputStream(jSocket.getOutputStream());
				
				try{
					outputToClient.write(filestatus.getBytes(), 0, 1);
				
					//Write to the socket's outputstream using the buffered output stream every byte in the byte array
					//Using bytes is important because this program is supposed to exchange all types of files not just txt files
					outputToClient.write(output, 0, output.length);
					System.out.println("Writing the file");
				}catch(IOException e){
					System.out.println("The client closed the connection unexpectedly");
				}
				
				
				//Flush everything on the buffer on to the output stream of the socket 
				outputToClient.flush();
				
				System.out.println("Socket closed");
			}
			else
			{
				//Obtain the socket's output stream and put it in a buffered output stream
				BufferedOutputStream outputToClient = new BufferedOutputStream(jSocket.getOutputStream());
				
				//Send the client the filestatus, "n" in this case because the file is not found
				outputToClient.write(filestatus.getBytes(), 0, 1);
				
				//If the file not found let the server admin know and close the socket.
				System.out.println("File not found");
			}
			
			//Close the socket
			jSocket.close();
			
		}
	}
}
