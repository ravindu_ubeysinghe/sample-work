import java.io.IOException; // Import java exception class
import java.net.*; // Import java networking package
import java.util.*; // Import java utility package

/**
 * @author Ravindu, 4956567
 * This class sends keeps listening to any incoming connections from a client and upon connection sends out the client a greeting message
 *
 */


public class UDPServer
{
	public static final String SERVERMESSAGE = "Hello, my name is Ravindu and my id is 4956567";
	/**
	 * @param args - command line arguments
	 * @throws SocketException
	 * @throws InterruptedException
	 */
    public static void main (String [] args) throws SocketException, InterruptedException
    {
    	//Creating a local variable for the port no
    	int portNo = 0;
    	
    	//Checking if the port no has been entered as a command line argument
    	if(args.length != 0){
    		//Checking if the port no is valid
            portNo = Integer.parseInt(args[0]);
            
            if(portNo < 1024 && portNo > 65535){
            	System.out.println("You've used a well known port or an invalid port, system exiting!");
            	Thread.sleep(5000);
            	System.exit(0);
            }
    	}
    	else
    	{
    		System.out.println("You should provide a command line argument containing the server port number, system exiting!");
    		Thread.sleep(5000);
    		System.exit(0);
    	}
        
    	
    	System.out.println("Server Listening");
        //Creating a UDP socket for the specified port by the user
        DatagramSocket socket = new DatagramSocket(portNo); 
        byte[] packetDataIn = new byte[1024]; //Creating a byte array for incoming data
        byte[] packetDataOut = new byte[1024]; //Creating a byte array for outgoing data
        
        while(true)
        {
           // Creating a UDP packet for incoming data
           DatagramPacket packetIn = new DatagramPacket(packetDataIn, packetDataIn.length);
          
           //Try to keep listening to port for any incoming connections if fails throw the exception
           try {
        	   socket.receive(packetIn);
           } catch (IOException e) {
        	   socket.close(); // if in case receiving fails close the socket because the execution will be stopped here
        	   System.out.println("An exception has occurred: " + e.getMessage()); // if an exception has occurred let the user know
           }
           
           //String messageIn = new String(packetIn.getData()); // Get the data from the received packet, combine all the bytes together and store them in a string
           InetAddress ipIn = packetIn.getAddress(); // Get the ip address from the received packet
           int portIn = packetIn.getPort(); // Get the port number from the recieved packet
           
          // System.out.println("Client says: " + messageIn); // Output the recieved message
           
           packetDataOut = SERVERMESSAGE.getBytes(); // Breakdown the server message which is a constant in to bytes and store them in the out going data buffer
           
           //Creating a packet to send to the client with the obtained ip address and the port number
           DatagramPacket packetOut = new DatagramPacket(packetDataOut, packetDataOut.length, ipIn, portIn);
          
           // Try and send the packet to the client if fails throw an exception
           try {
        	   socket.send(packetOut);
           } catch (IOException e) {
        	   socket.close(); // if in case sending fails close the socket because the execution will be stopped here
        	   System.out.println("An exception has occurred: " + e.getMessage()); // if an exception has occurred let the user know
           }
           
        }
        
    }
}
