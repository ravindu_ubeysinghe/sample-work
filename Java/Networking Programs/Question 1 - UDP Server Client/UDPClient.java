import java.io.IOException; // Import java exception class
import java.net.*; // Import java networking package
import java.util.*; // Import java utility package



/**
 * @author Ravindu, 4956567
 * This is the UDPClient class which receives the UDPServer's greeting message upon the connection
 *
 */

public class UDPClient 
{
	/**
	 * 
	 * @param args - command line arguments
	 * @throws SocketException
	 * @throws InterruptedException
	 */
	public static void main(String [] args) throws SocketException, InterruptedException
	{
		//Creating local variables for the port no, server Ip and greeting message 
		int portNo = 0; 
		String serverIp = " ", clientGreeting = "Hello, server!";
		
		//Checking if the port no and server ip have been entered as a command line arguments
    	if(args.length == 2){
    		
    		serverIp = args[0];
    		
    		//Checking if the port no is valid
            portNo = Integer.parseInt(args[1]);
            
            if(portNo < 1024 && portNo > 65535){
            	System.out.println("You've used a well known port or an invalid port, system exiting!");
            	Thread.sleep(5000);
            	System.exit(0);
            }
    	}
    	else
    	{
    		System.out.println("You should provide command line arguments containing the server port number and ip, system exiting!");
    		Thread.sleep(5000);
    		System.exit(0);
    	}
        
        
        //Creating a UDP socket for any available port on the PC
        DatagramSocket socket = new DatagramSocket(); 
        byte[] packetDataIn = new byte[1024]; //Creating a byte array for incoming data
        byte[] packetDataOut = new byte[1024]; //Creating a byte array for outgoing data
        
        packetDataOut = clientGreeting.getBytes(); // Breaking the client greeting message in to bytes (characters; 1 char = 1 byte) 
        InetAddress ipOut = null; // Initializing the ip address to null because input from user may not be correct, in that way ip address may not be initialized at all
		//Try to obtain the ip address from the user input if not successful throw an exception in the catch block
        try {
			ipOut = InetAddress.getByName(serverIp); //get the ip based on the host name given by the user
		} catch (UnknownHostException e1) {
			System.out.println("An exception has occurred: " + e1.getMessage()); // if an exception has occurred let the user know
		}
        
        //Create a new UDP packet with output data, ip address of the server and the port number
        DatagramPacket packetOut = new DatagramPacket(packetDataOut, packetDataOut.length, ipOut, portNo);
        
        //Try to send the packet to the server if fails throw an exception in the catch block
        try {
			socket.send(packetOut);
		} catch (IOException e) {
			socket.close(); // if in case sending fails close the socket because the execution will be stopped here
	       	System.out.println("An exception has occurred: " + e.getMessage()); // if an exception has occurred let the user know
		}
        
        //Create a UDP packet for the receiving data
        DatagramPacket packetIn = new DatagramPacket(packetDataIn, packetDataIn.length);
        
        //Try to receive data from the server if not throw an exception in the catch block
        try {
			socket.receive(packetIn);
		} catch (IOException e) {
			socket.close(); // if in case sending fails close the socket because the execution will be stopped here
			System.out.println("An exception has occurred: " + e.getMessage()); // if an exception has occurred let the user know
		}
        
        String messageIn = new String(packetIn.getData()); // Get the data from the received packet, combine all the bytes together and store them in a string
        
        System.out.println("Server says: " + messageIn); // Print the string out to the command line
        
        socket.close(); // Close the socket as it's no longer used and if not closed no other program will be able to use it until the PC is restarted
        	
	}
}
