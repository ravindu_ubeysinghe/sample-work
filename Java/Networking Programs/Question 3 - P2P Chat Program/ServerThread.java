import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.*;


/**
 * @author Ravindu, 4956567
 * @category Thread, sub class
 * This is the server thread that server class is going to launch everytime a client gets connected to the server
 * This is handling getting the output from the passed in socket and display the clients' messages on the screen
 */

public class ServerThread extends Thread {
	Socket jSocket; //Instance variable for the socket
	String username; //Instance variable for the username that's passed in through the constructor
	String ip; //Instance variable for the formatted ip address that's passed in throught the constructor
	
	/**
	 * 
	 * Constructor for the server thread, through which the class is initialized
	 * @param in - Socket passed in by the server
	 * @param usernameIn - The username of the ip address owner obtained and passed by the server
	 * @param ipIn - Formatted IP address passed by the server
	 *
	 */
	public ServerThread(Socket in, String usernameIn, String ipIn)
	{
		//Store the passed in parameters in the corresponding local variables
		jSocket = in;
		username = usernameIn;
		ip = ipIn;
	}
	
	/**
	 * 
	 * This method gets executed once the thread is created and the start() method is called
	 * This is where the real activities of the thread are occurred during its runtime
	 *
	 */
	public void run() {
		try {
			//Get the input stream from the socket and store it in a buffered reader
			BufferedReader br = new BufferedReader(new InputStreamReader(jSocket.getInputStream()));
			
			//Declaring a variable for the message to be obtained from the socket
			String messageIn = null;
			System.out.println("\n" + username + "<IP " + ip + "> : " + "connected"); //Let the user know of anyone who's connected
			
			//Try to get the output from the socket and print it on the screen, if incase the other peer closes the connection unexpectedly
			//catch the exception and print that the peer was disconnected
			try
			{
				//While the buffered reader reads something, isn't equal to null, run this loop and output it to the screen
				while((messageIn = br.readLine()) != null)
				{
					System.out.println("\n" + username + "<IP " + ip + "> : " + messageIn);
				}
			}catch(IOException e){
				System.out.println("\n" + username + "<IP " + ip + "> : " + "disconnected");
				jSocket.close(); //Peer disconnected, hence close the corresponding socket
			}
			
			//Close the socket
			jSocket.close();
			
			//Catch the exception if occured and let the user know the details
		} catch (IOException e) {
			e.printStackTrace();
		}		
	}

}
