import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;


/**
 * @author Ravindu, 4956567
 * A single thread class that's responsible for deploying server threads once a connection has been established with relevant information
 *
 */

public class Server extends Thread {
	
	//Creating all the instance variables; for server socket, incoming client socket, peerlist, boolean for if the ip is found in the peerlist
	//And username for the name of the owner of the corresponding ip address
	ServerSocket tcpSocket;
	Socket jSocket;
	String[][] peerList;
	boolean found;
	String username;
	
	//An array list for keeping track of the unauthorized ips
	List<String> unauthorizedIps = new ArrayList<String>();
	
	/**
	 * 
	 * Constructor for the server, through which the class is initialized
	 * @param in - The server socket passed in from the driver
	 * @param peerListIn - The two dimensional array of ip address and usernames passed by the driver 
	 *
	 */
	public Server(ServerSocket in, String[][] peerListIn)
	{
		//Initializing local variables with the paramter values
		tcpSocket = in;
		peerList = peerListIn;
	}
	
	/**
	 * 
	 * Run method for the server which waits for a client to get connected, obtain the incoming socket and pass it to the server thread
	 * Along with extracted relevant information like the ip and the username
	 *
	 */
	public void run()
	{
		//Constantly keep checking if a client wants to connect
		while(true)
		{
			found = false;
			
			//Try and accept a client's connection if not throw an exception
			try {
				jSocket = tcpSocket.accept();
			} catch (IOException e) {
				e.printStackTrace();
			}
			
			//Format the ip address that's obtained through the InetAddress.toString() method to not have the / and store it in a string
		    String ip = jSocket.getInetAddress().toString().substring(1);
		    
		    //Browsing through the peer list to check if the ip address from which the request has come is in the peer list
		    for(int i=0; i < peerList.length; i++)
		    {
		    	//If found set found to true and obtain the corresponding username
		    	if(ip.equalsIgnoreCase(peerList[i][1]))
		    	{
		    		found = true;
		    		username = peerList[i][0];
		    	}
		    }
		    
		    //If found is true launch a new sever thread along with the incoming socket, the username and the formatted ip
		    if(found)
		    {
		    	new ServerThread(jSocket, username, ip).start();
		    }
		    else
		    {
		    	//If not and if the ip isn't found in the unauthorized ip list as well output the following message and add it to the
		    	//Unauthorized ip list
		    	if(!unauthorizedIps.contains(ip))
		    	{
		    		System.out.println("\nUnauthorized chart request from <IP " + ip + ">");
		    		unauthorizedIps.add(ip);
		    	}
		    	//If found in the unauthorized ip list, do nothing
		    	else{}
		    }
		}
	}
}
