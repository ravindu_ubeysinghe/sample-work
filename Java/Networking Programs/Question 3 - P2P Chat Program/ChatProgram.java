import java.util.*;
import java.io.*;
import java.net.*;

/**
 * @author Ravindu, 4956567
 * The main class which is used as the driver of the P2P program
 * Launch both the server and class threads so that they can run on their own
 */

public class ChatProgram 
{
	static ServerSocket tcpSocket = null; //Creating a server socket for the server
	
	/**
	 * @return an array list of Strings that contains all the lines of the peerlist file
	 * This function loads the peer list file and returns an array list of the content
	 *
	 */
	public static List<String> loadPeerFile() throws IOException
	{
		//Buffered reader to read from the file
		BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(new File("Peerlist.txt"))));
		
		//Creating an array list to store the file content
		List<String> peerlist = new ArrayList<String>();
		
		//Read each line from the file while the buffered reader has content in its buffer; ready to be read
		while(br.ready())
		{
			peerlist.add(br.readLine()); //Add line to the array list
		}
		
		return peerlist; //Return the array list
	}
	
	
	/**
	 * 
	 * This method reads from the content of the two dimensional array that it's passed in 
	 * Two Dimensional Array [index][name , ip address]
	 *
	 */
	public static void showClients(String ipList[][])
	{
		System.out.println("Following peers are online!\n");
		
		//Keep displaying each peer until the end of the array
		for(int i=0; i<ipList.length; i++)
		{
			System.out.println("Peer " + (i+1) + ", " + ipList[i][0] + "<" + ipList[i][1] + ">");
		}
	}

	/**
	 * 
	 * This is the main method which loads the peer file, put the content in to a two dimensional array and launch the client and server threads
	 * @throws IOException
	 */
	public static void main(String [] args) throws IOException
	{
		List<String> peerlist = loadPeerFile(); //Loading the peer file
		String ipList[][] = new String[peerlist.size()][2]; //Declaring the two dimensional array to store the input

		//Keep storing data to the array until the end of the peerlist's length
		for(int i=0; i < peerlist.size(); i++)
		{
			String temp[] = peerlist.get(i).split("\\s+");
			ipList[i][0] = temp[0];
			ipList[i][1] = temp[1];
		}
		
		//Print all the clients to the screen
		showClients(ipList);
		
		//Creating a new server socket for the server
		tcpSocket = new ServerSocket(4035);
		
		new Server(tcpSocket, ipList).start(); //Launching the server thread
		new ClientThread(ipList).start(); //Launching the client thread
		
		
	}
}
