import java.io.*;
import java.net.*;
import java.util.Scanner;


/**
 * @author Ravindu, 4956567
 * @category Thread, sub class
 * This class is where the input from the user is read and sent to all the online peers while the server is being run at the other end
 */

public class ClientThread extends Thread {
	
	//Creating all the instance variables necessary, Arrays of sockets, print writers, a peerList and a buffered reader
	Socket jSocket[];
	PrintWriter pr[];
	BufferedReader br;
	String peerList[][];
	Scanner keyboard;
	
	/**
	 * 
	 * Constructor for the client thread, through which the class is initialized
	 * @param peerListIn - The two dimensional array of ip address and usernames passed by the driver 
	 * @exception UnknownHostException, IOException
	 *
	 */
	public ClientThread(String[][] peerListIn) throws UnknownHostException, IOException
	{
		keyboard = new Scanner(System.in);
		peerList = peerListIn; //Assigning the passed in array of peer details to the local variable
		jSocket = new Socket[peerList.length]; //Creating an array of sockets for all the peers
		pr = new PrintWriter[peerList.length]; //Creating an array of print writers for all the peers
		
		//Loop until the end of the peerlist array and initialize all the sockets and the print writers
		for(int i=0; i<peerList.length; i++)
		{
			//variable to handle socket resetting
			boolean reset = true;
			
			//While waiting for other peers to connect these created sockets may timeout if so an exception will be caught and the sockets will be reset
			//until the other peers successfully connect
			while(reset)
			{
				try{
					jSocket[i] = new Socket(peerList[i][1], 4035);
					pr[i] = new PrintWriter(new DataOutputStream(jSocket[i].getOutputStream()));
					reset = false; //exit the loop since the peer has been connected successfully no further exceptions will be thrown
				}catch(ConnectException e){
					reset = true;
				}
			}
			
		}
	}
	
	/**
	 * 
	 * This method gets executed once the thread is created and the start() method is called
	 * This is where the input from the console is read and sent to all the online peers
	 *
	 */
	public void run(){
		//Constantly looking for whatever's being typed and if anything's typed send it to all the peers
		while(true)
		{
			//Getting the ip address of this machine and storing it in localIp variable
			String localIp = "";
			try {
				localIp = Inet4Address.getLocalHost().getHostAddress().toString();
			} catch (UnknownHostException e) {
				e.printStackTrace();
			}
			
			//Loop through the peerlist and find the username of this machine
			String myUsername = "Rouge Client";
			
			for(int i=0; i < peerList.length; i++){
				if(peerList[i][1].equalsIgnoreCase(localIp)){
					myUsername = peerList[i][0];
					break;
				}
			}
			
			//Get the input from the user in the correct format 
			String in = keyboard.nextLine();
			if(in != ""){
				System.out.println(myUsername + "<" + localIp + "> : " + in);
			}
			
			//Loop through the peer list and send the typed in message to all the peers
			//Make sure to not to send the messages sent by this machine to this machine 
			for(int i=0; i<peerList.length; i++)
			{
				if(!peerList[i][1].equalsIgnoreCase(localIp))
				{
					pr[i].println(in);
					pr[i].flush();
				}
			}
		}
	}
}
