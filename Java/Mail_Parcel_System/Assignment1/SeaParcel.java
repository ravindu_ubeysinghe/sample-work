package Assignment1;

import java.io.*;
import java.util.*;

import javax.swing.*;

/**
 * The class representing the physical object of Sea Parcel sub-class of International Post Item
 * @author Ravindu
 * @version 1.0 10/10/2014
 */

public class SeaParcel extends InternationalPostItem {
	private String destination;
	private double weight;
	private double price;
	private double maxWeight;
	private String zone = "";
	private ArrayList<String> priceList;
	private ArrayList<String> destinationList;
	private ArrayList<String> weightList;
	private final String type = "Parcel Sea";
	int parcelid = (int)(Math.random()*1000);
	
	/**
	 * Empty constructor of the Sea Parcel class which initializes the values to 0 and/or "" and calls the super class object
	 */
	public SeaParcel(){
		super();
		destination = "";
		weight = 0;
		price = 0;
		maxWeight= 0;
	}
	
	/**
	 * 
	 * Overloaded constructor which gets 3 parameters to initialize the variables and Calculates the price afterwards using calcSpPrice method
	 * 
	 * @param weight - Weight of the Sea Parcel
	 * @param destination - Destination to which the Sea Parcel has to be sent
	 * @param maxWeight - Maximum weight for Sea Parcel obtained from the file 
	 */
	public SeaParcel(double weight, String destination){
		super();
		this.destination = destination;
		this.weight = weight;
		this.price = 0;
		calcSpPrice();
	}
	
	/**
	 * Method to return each Sea Parcel's unique ID
	 * 
	 * @return unique ID identify Sea Parcel
	 */
	public int getId(){
		return parcelid;
	}
	
	/**
	 * Method to set the destination of the Sea Parcel
	 *
	 * @param destination - Destination for the Sea Parcel
	 */
	public void setDestination(String destination){
		this.destination = destination;
	}
	
	/**
	 * Sets the weight of the Sea Parcel
	 * 
	 * @param weight - The weight of the Sea Parcel
	 */
	public void setWeight(double weight){
		this.weight = weight;
	}
	
	/**
	 * Sets the price of the Sea Parcel
	 * 
	 * @param price - The price of the Sea Parcel
	 */
	public void setPrice(double price){
		this.price = price;
	}
	
	/**
	 * Method used to get the value of destination of the Sea Parcel
	 * 
	 * @return Destination of the Sea Parcel
	 */
	public String getDestination(){
		return this.destination;
	}
	
	/**
	 * Method used to get the value of weight of the Sea Parcel
	 * 
	 * @return The weight of the Sea Parcel
	 */
	public double getWeight(){
		return this.weight;
	}
	
	/**
	 * Method used to get the price of the Sea Parcel
	 * 
	 * @return The price of the Sea Parcel
	 */
	public double getPrice(){
		return this.price;
	} 
	
	/**
	 * Method used to calculate the price of the Sea Parcel
	 */
	public void calcSpPrice() {
		this.priceList = super.loadList("PriceParcelAir.txt");
		//this.weightList = super.loadList("WeightParcelAir.txt");
		this.destinationList = super.loadList("Destination.txt");
		
		double weighting = weight / 1000; //converting to grams
		
		zone = super.searchZone(destinationList, destination);
		//maxWeight = super.searchMaxWeight(weightList, destination);
		price = super.searchPrice(priceList,zone , weighting, maxWeight);
	}
	
	/**
	 * Method used to printout the details of Sea Parcel to a file
	 * 
	 * @param out - output file stream
	 */
	public void printDetails(PrintWriter out){
		if (type != "" && destination != "" && weight != 0 && price != 0){
			super.write(out, type, destination, weight, price);
			JOptionPane.showMessageDialog(null, "Sea Parcels you selected/purchased have been written to the Invoice/Quotation successfully", "Notice", JOptionPane.INFORMATION_MESSAGE);
		}else{
			JOptionPane.showMessageDialog(null, "Error writing Sea Parcels information to the Invoice/Quotation", "Error", JOptionPane.ERROR_MESSAGE);
		}
	}
}

