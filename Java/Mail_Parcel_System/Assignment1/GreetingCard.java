package Assignment1;

import java.io.*;
import java.util.*;

import javax.swing.*;

/**
 * The class representing the physical object of Greeting Card sub-class of International Post Item
 * @author Chris
 * @version 1.0 10/10/2014
 */

public class GreetingCard extends InternationalPostItem{
	private String destination;
	private double weight;
	private double price;
	private double maxWeight;
	private String seasson;
	private ArrayList<String> gcprice;
	private ArrayList<String> gcsprice;
	private final String type = "Greeting Card";
	private double weighting;
	int parcelid = (int)(Math.random()*100000);

	private String gcmonth;
	
	/**
	 * Empty constructor of the Greeting Card class which initializes the values to 0 and/or "" and calls the super class object
	 */
	public GreetingCard(){
		gcmonth = "";
		destination = "";
		weight = 0;
		price = 0;
		 
	}
	
	/**
	 * 
	 * Overloaded constructor which gets 3 parameters to initialize the variables and Calculates the price afterwards using calcPrice method
	 * 
	 * @param weight - Weight of the Greeting Card
	 * @param destination - Destination to which the Greeting Card has to be sent
	 * @param gcmonth - Month in which the Greeting Card has to be sent
	 */
	public GreetingCard(double weight, String destination, String gcmonth) {
		super();
		this.destination = destination;
		this.weight = weight;
		this.price = 0 ;
		this.gcmonth = gcmonth;
		calcGCPrice();
		
	}
	
	/**
	 * Method to set the season of the Greeting Card
	 *
	 * @param destination - Destination for the Greeting Card
	 */
	public void setSeasson(String seasson){
		this.seasson = seasson;
	}

	/**
	 * Method to set the destination of the Greeting Card
	 *
	 * @param destination - Destination for the Greeting Card
	 */
	public void setDestination(String destination){
		this.destination = destination;
	}

	/**
	 * Sets the weight of the Greeting Card
	 * 
	 * @param weight - The weight of the Greeting Card
	 */
	public void setWeight(double weight){
		this.weight = weight;
	}
	
	/**
	 * Sets the price of the Greeting Card
	 * 
	 * @param price - The price of the Greeting Card
	 */
	public void setPrice(double price){
		this.price = price;
	}
	
	/**
	 * Method to return each Greeting Card's unique ID
	 * 
	 * @return unique ID identify Greeting Card
	 */
	public int getId(){
		return parcelid;
	}
	
	/**
	 * Gets the destination of the Greeting Card
	 * 
	 * @param destination - The destination of the Greeting Card
	 */
	public String getDestination(){
		return this.destination;
	}

	/**
	 * Method used to get the value of weight of the Greeting Card
	 * 
	 * @return The weight of the Greeting Card
	 */
	public double getWeight(){
		return this.weight;
	}
	
	/**
	 * Method used to find the season and return the value of season
	 * 
	 * @return The weight of the Greeting Card
	 */
	public String getSeasson(){
		if(gcmonth.equalsIgnoreCase("November") || gcmonth.equalsIgnoreCase("December")){
			seasson = "YES";
			}else
				seasson= "NO";
		return this.seasson;
	}

	/**
	 * Method used to get the price of the Greeting Card
	 * 
	 * @return The price of the Greeting Card
	 */
	public double getPrice(){
		
		return this.price;
	}
	

	/**
	 * Method used to calculate the price of the Greeting Card
	 * @exception NullPointerException for season
	 */
	public void calcGCPrice() {
		this.gcprice = super.loadList("PriceGreetingCard.txt");
		this.gcsprice = super.loadList("PriceGreetingCardSeasonal.txt");


		getSeasson();
		maxWeight = super.maxWeight(gcprice);
		weighting = weight / 100; 
			

		try{
			if(this.seasson.equals("YES")){
				price = super.searchGCSPrice(gcsprice);
			}
			}catch(NullPointerException e){
				getSeasson();
			}
			
			
			
		
			try{
				if(this.seasson.equals("NO")){
					price = super.searchGCPrice(gcprice);
				}
			
			}catch(NullPointerException e){
				 getSeasson();	
			}
	}
	
	/**
	 * Method used to printout the details of Greeting Card to a file
	 * 
	 * @param out - output file stream
	 */
	public void printDetails(PrintWriter out){
		if (type != "" && destination != "" && weight != 0 && price != 0){
			super.writegc(out, type, seasson, weight, price);
			JOptionPane.showMessageDialog(null, "Greeting Cards you selected/purchased have been written to the Invoice/Quotation successfully", "Notice", JOptionPane.INFORMATION_MESSAGE);
		}else{
			JOptionPane.showMessageDialog(null, "Error writing Greeting Cards to the Invoice/Quotation", "Error", JOptionPane.ERROR_MESSAGE);
		}

	}	
	
	
}