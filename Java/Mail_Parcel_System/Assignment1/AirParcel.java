package Assignment1;

import java.io.*;
import java.util.*;

import javax.swing.*;

/**
 * The class represnting the physical object of Air Parcel sub-class of International Post Item
 * @author Ravindu
 * @version 1.0 10/10/2014
 */

public class AirParcel extends InternationalPostItem {
	private String destination;
	private double weight;
	private double price;
	private double maxWeight;
	private double weighting;
	private String zone = "";
	private ArrayList<String> priceList;
	private ArrayList<String> destinationList;
	private ArrayList<String> weightList;
	private final String type = "Parcel Air";
	int parcelid = (int)(Math.random()*100);
	
	/**
	 * Empty constructor of the Air Parcel class which initializes the values to 0 and/or "" and calls the super class object
	 */
	public AirParcel(){
		super();
		destination = "";
		weight = 0;
		price = 0;
		maxWeight= 0;
		weighting = 0;
	}
	
	/**
	 * 
	 * Overloaded constructor which gets 3 parameters to initialize the variables and Calculates the price afterwards using calcPrice method
	 * 
	 * @param weight - Weight of the Air Parcel
	 * @param destination - Destination to which the Air Parcel has to be sent
	 * @param maxWeight - Maximum weight for Air Parcel obtained from the file 
	 */
	public AirParcel(double weight, String destination, double maxWeight){
		super();
		this.destination = destination;
		this.weight = weight;
		this.price = 0;
		this.weighting = weight/1000;
		this.maxWeight = maxWeight;
		calcPrice();
	}
	
	/**
	 * Method to return each Air Parcel's unique ID
	 * 
	 * @return unique ID identify Air Parcel
	 */
	public int getId(){
		return parcelid;
	}
	
	/**
	 * Method to set the destination of the Air Parcel
	 *
	 * @param destination - Destination for the Air Parcel
	 */
	public void setDestination(String destination){
		this.destination = destination;
	}
	
	/**
	 * 
	 * Obtains the maximum wieght of Air Parcel and compares it with the use input
	 * 
	 * @deprecated from stage 1, uses Validate class validWeight method to find the max weight and compare it with the given weight
	 * 
	 * @return Max Weight of the Air Parcel 
	 */
	public boolean getMaxWeight(){
		this.weightList = super.loadList("WeightParcelAir.txt");
		maxWeight = super.searchMaxWeight(weightList, destination);
		if (!(weighting <= maxWeight)){
			JOptionPane.showMessageDialog(null, "This weight is invalid for " + destination, "Error", JOptionPane.ERROR_MESSAGE);
			return false;
		}else{
			return true;
		}
	}
	
	/**
	 * Sets the weight of the Air Parcel
	 * 
	 * @param weight - The weight of the Air Parcel
	 */
	public void setWeight(double weight){
		this.weight = weight;
	}
	
	/**
	 * Sets the price of the Air Parcel
	 * 
	 * @param price - The price of the Air Parcel
	 */
	public void setPrice(double price){
		this.price = price;
	}
	
	/**
	 * Method used to get the value of destination of the Air Parcel
	 * 
	 * @return Destination of the Air Parcel
	 */
	public String getDestination(){
		return this.destination;
	}
	
	/**
	 * Method used to get the value of weight of the Air Parcel
	 * 
	 * @return The weight of the Air Parcel
	 */
	public double getWeight(){
		return this.weight;
	}
	
	/**
	 * Method used to get the price of the Air Parcel
	 * 
	 * @return The price of the Air Parcel
	 */
	public double getPrice(){
		return this.price;
	} 
	
	/**
	 * Method used to calculate the price of the Air Parcel
	 */
	public void calcPrice() {
		this.priceList = super.loadList("PriceParcelAir.txt");
		//this.weightList = super.loadList("WeightParcelAir.txt");
		this.destinationList = super.loadList("Destination.txt");
		
		double weighting = weight / 1000; //converting to grams
		
		zone = super.searchZone(destinationList, destination);
		//maxWeight = super.searchMaxWeight(weightList, destination);
		price = super.searchPrice(priceList,zone , weighting, maxWeight);
	}
	
	/**
	 * Method used to printout the details of Air Parcel to a file
	 * 
	 * @param out - output file stream
	 */
	public void printDetails(PrintWriter out){
		if (type != "" && destination != "" && weight != 0 && price != 0){
			super.write(out, type, destination, weight, price);
			JOptionPane.showMessageDialog(null, "Air Parcels you selected/purchased have been written to the Invoice/Quotation successfully", "Notice", JOptionPane.INFORMATION_MESSAGE);
		}else{
			JOptionPane.showMessageDialog(null, "Error writing Air Parcels information to the Invoice/Quotation", "Error", JOptionPane.ERROR_MESSAGE);
		}
	}
}

