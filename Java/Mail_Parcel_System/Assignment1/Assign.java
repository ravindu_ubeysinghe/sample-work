package Assignment1;


import MachineGUI.*;

import java.text.*;
import java.util.*;

import javax.swing.*;
import javax.swing.Timer;

import java.awt.*;
import java.awt.List;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.*;
/**
 * 
 * @author Ravindu
 * @version 1.0 2014/10/10
 * The main class of Stamping Machine which contains the public static void main method
 */


public class Assign {
	
	//DECLARATION OF VARIABLES
	private ArrayList <AirParcel> apList = new ArrayList<AirParcel>();
	private ArrayList <GreetingCard> gcList = new ArrayList<GreetingCard>();
	private ArrayList <LetterAir> laList = new ArrayList<LetterAir>();
	private ArrayList <SeaParcel> spList = new ArrayList<SeaParcel>();
	private JFrame frame = new JFrame("Stamping Machine");
	public static boolean result = false;
	private boolean paid = false;
	private String type;
	private static final int TIMER_PERIOD = 1000;
    protected static int maxCount = 10;
    private static int count;
	
    /**
     * 
     * @param args
     * 
     * The main method
     * @exception NumberFormatException for Quantity, Weight and Destination
     * @exception IOException for file writing
     */
	public static void main(String [] args){
		
		int quantity = 0, userContinue = 0, machineContinue = 0, option, cartOption;
		String destination = "", month = "";
		double weight = 0, maxWeight = 0;
		boolean writeSuccessful = false, machineContinueBool = true, userContinueBool = true, correctMonth = false, shoppingCartExists = false, isAN = true;
		
		InternationalPostItem ipi = new InternationalPostItem();
		Validate v = new Validate();
		
		ArrayList <String> mainOptionsList = new ArrayList<String>();
		//GENERATING MAIN OPTIONS LIST
		mainOptionsList.add("Air Parcel");
		mainOptionsList.add("Sea Parcel");
		mainOptionsList.add("Greeting Card");
		mainOptionsList.add("Letter Air");
		String[]mainOptions;
		
		//GENERATING THE SHOPPING CART OPTIONS LIST
		String [] shoppingOptions = {"View Cart", "Delete Item", "Checkout"};
		
		// GETTING CURRENT DATE AND TIME TO GENERATE THE FILENAME AND THE HEADER
		DateFormat df = new SimpleDateFormat("E MMM d HH mm ss z y");
		Calendar calobj = Calendar.getInstance();
		String outputFileName = df.format(calobj.getTime()) + ".txt";
		String outputHeader = df.format(calobj.getTime());
		
		//CREATING AN OBJECT OF THE SAME CLASS
		Assign sm = new Assign();
		
		do{
			MessageDialog md = new MessageDialog("Welcome!","Welcome to the Stamping Machine, Click OK to continue");
			do{}while(md.getInput() == false);
			do{
				
					// ADDING THE SHOPPING CART OPTION TO THE MAIN OPTIONS IF AT LEAST ONE PRODUCT HAS BEEN ADDED TO THE CART
					for(int i = 0; i < mainOptionsList.size(); i++){
						if(mainOptionsList.get(i).equalsIgnoreCase("Shopping Cart")){
							shoppingCartExists = true;
						}
					}
					if((sm.getAplist().size() == 0 && sm.getGclist().size() == 0 && sm.getLalist().size() == 0 && sm.getSplist().size() == 0) || shoppingCartExists == true){
						mainOptions = new String[mainOptionsList.size()];
						mainOptions = mainOptionsList.toArray(mainOptions);
					}else{
						mainOptionsList.add("Shopping Cart");
						mainOptions = new String[mainOptionsList.size()];
						mainOptions = mainOptionsList.toArray(mainOptions);
					}
					OptionDialog op = new OptionDialog( "Select Parcel Type", "Which parcel type do you want to get a stamp for?", mainOptions);
					do{}while(op.getInput() == null);
					option = op.getButton();
				
				//IF THE SHOPPING CART OPTION IS SELECTED, OPENING THE SHOPPING CART MENU
				if(option == 4){
					OptionDialog cart = new OptionDialog("Shopping Cart", "Please select an option", shoppingOptions);
					do{}while(cart.getInput() == null);
					cartOption = cart.getButton();
					
					switch(cartOption){
					case 0: sm.showCart();
							break;
					case 1: sm.showCart();
							sm.deleteItem();
							break;
					case 2:
						sm.showCart();
						sm.checkout();
						sm.paid = true;
						break;
							
					}
				}else{
					do{
						try{
							InputDialog qtyid = new InputDialog("Stamping Machine", "What is the quantity?");
							quantity = Integer.parseInt(qtyid.getInput());
							isAN = true;
						}catch(NumberFormatException e){
							MessageDialog qtyerr = new MessageDialog("Error", "The quantity should be a number and greater than 0");
							do{}while(qtyerr.getInput() == false);
							isAN = false;
						}
					}while(quantity == 0 || isAN == false);
					
					for(int i=0; i<quantity; i++){
						do{
							InputDialog desid = new InputDialog("Stamping Machine","What is the destination " + (i+1) + "?");
							destination = desid.getInput();
							if(destination.equalsIgnoreCase("")){
								MessageDialog deserr = new MessageDialog("Error", "Please enter a destination");
								do{}while(deserr.getInput() == false);
							}
						}while(destination.equalsIgnoreCase("") ||v.validDestination(destination)==false); // has be a return value from a function in a different class
						
						//IF GreetingCard SELECTED USING AN ALTERNATIVE VALIDATION METHOD TO CHECK THE VALIDITY OF THE WEIGHT
						if(option == 2){
							do{
								try{
									InputDialog weightid = new InputDialog("Stamping Machine", "What is the weight of the item in grams " + (i+1) + "?");
									weight = Double.parseDouble(weightid.getInput());
								}catch(NumberFormatException e){
									MessageDialog weighterr = new MessageDialog("Error", "Please enter a double as the weight");
									do{}while(weighterr.getInput() == false);
								}
							}while(weight == 0 || v.validGCWeight(weight) == false );
						}else{
							do{
								try{
									InputDialog weightid = new InputDialog("Stamping Machine", "What is the weight of the item in grams" + (i+1) + "?");
									weight = Double.parseDouble(weightid.getInput());
								}catch(NumberFormatException e){
									MessageDialog weighterr = new MessageDialog("Error", "Please enter a double as the weight");
									do{}while(weighterr.getInput() == false);
								}
							}while(weight == 0 || v.validWeight(weight, destination) == false );
						}
						
						
						
						// CHECKING WHICH OPTION HAS BEEN SELECTED AND PERFORM ACTIONS ACCORDINGLY
						switch(option){
							case 0:	sm.getAplist().add(new AirParcel(weight, destination, maxWeight));
								break;
							case 1: sm.getSplist().add(new SeaParcel(weight, destination));
								break;
							case 2: 
								do{
									InputDialog monthid = new InputDialog("Stamping Machine","Please enter the month for item " + (i+1) + "?");
									month = monthid.getInput();
									if(month.equalsIgnoreCase("")){
										MessageDialog montherr = new MessageDialog("Error", "Please enter a month");
										do{}while(montherr.getInput() == false);
								}	
								}while(month.equalsIgnoreCase("") || v.validMonth(month) == false);
								
								sm.getGclist().add(new GreetingCard(weight, destination, month));
								
								break;
							case 3: sm.getLalist().add(new LetterAir(weight, destination));
								break;
							case 4: sm.showCart();
								break;
						} 
						
					}
				}
				
				userContinue = JOptionPane.showOptionDialog(null, 
								"Press 'YES' to continue shopping or Press 'NO' end shopping & print the invoice or quotation", 
								"Stamping Machine", 
								JOptionPane.YES_NO_OPTION, 
								JOptionPane.QUESTION_MESSAGE, 
								null, null, null);
								
				if(userContinue == JOptionPane.YES_OPTION){
					userContinueBool = true;
				}else{
					userContinueBool =false;
				}
			}while(userContinueBool);

				//WRITING THE INVOICE/QUOTATION IF THE USER DOESN'T WISH TO CONTINUE
				try{
					PrintWriter out = new PrintWriter(new FileWriter(outputFileName));
					String inquo = "";
					if(sm.paid){
						inquo = "Invoice";
					}else{
						inquo = "Quotation";
					}
					out.println(inquo + " On : " + outputHeader);
					out.println("-----------------------------");
					
					double apTotal = 0, gcTotal = 0, laTotal = 0, spTotal= 0, grandTotal = 0;
					
					for(int i=0; i <sm.getAplist().size(); i++){
						apTotal = apTotal + sm.getAplist().get(i).getPrice();
						grandTotal = grandTotal + apTotal;
					}
					for(int i=0; i <sm.getGclist().size(); i++){
						gcTotal = gcTotal + sm.getGclist().get(i).getPrice();
						grandTotal = grandTotal + gcTotal;
					}
					for(int i=0; i <sm.getLalist().size(); i++){
						laTotal = laTotal + sm.getLalist().get(i).getPrice();
						grandTotal = grandTotal + laTotal;
					}
					
					for(int i=0; i<sm.getSplist().size(); i++){
						spTotal = spTotal + sm.getSplist().get(i).getPrice();
						grandTotal = grandTotal + spTotal;
					}
					if(sm.getAplist().size() != 0){
						out.println("Parcel Air * " + sm.getAplist().size() + "\t" + apTotal);
					}
					if(sm.getGclist().size() !=0){
						out.println("Greeting Card * " + sm.getGclist().size() + "\t" + gcTotal);
					}
					if(sm.getLalist().size() != 0){
						out.println("Letter Air * " + sm.getLalist().size() + "\t" + laTotal);
					}
					if(sm.getSplist().size() != 0){
						out.println("Sea Parcel * " + sm.getSplist().size() + "\t" + spTotal);
					}
					
					out.println("Total: \t\t " + grandTotal);
					
					out.println("-----------------------------");
					out.println("List of Stamps");
					out.println("-----------------------------");
					
					for(int i=0; i <sm.getAplist().size(); i++){
						sm.getAplist().get(i).printDetails(out);
						out.println("-----------------------------");
					}
			
					for(int i=0; i <sm.getGclist().size(); i++){
						sm.getGclist().get(i).printDetails(out);
						out.println("-----------------------------");
					}
					for(int i=0; i <sm.getLalist().size(); i++){
						sm.getLalist().get(i).printDetails(out);
						out.println("-----------------------------");
					}
					for(int i=0; i<sm.getSplist().size(); i++){
						sm.getSplist().get(i).printDetails(out);
						out.println("-----------------------------");
					}
							
					out.close();
				}catch(IOException io){
					MessageDialog ioerr = new MessageDialog("Error","IO Error Occurred");
					do{}while(ioerr.getInput() == false);
					writeSuccessful = false;
				}
			
			machineContinue = JOptionPane.showOptionDialog(null, 
								"Shutdown Machine?", 
								"Stamping Machine", 
								JOptionPane.YES_NO_OPTION, 
								JOptionPane.QUESTION_MESSAGE, 
								null, null, null);
								
				if(machineContinue == JOptionPane.YES_OPTION){
					machineContinueBool = false;
				}else{
					machineContinueBool = true;
				}
		}while(machineContinueBool);	
		
		System.exit(0);
	}
	
	/**
	 * Method to view the shopping cart
	 */
	public void showCart(){
		ArrayList<String> output = new ArrayList<String>();
		double total =0;
		int item = 0;
		output.add(( "Item" + "   ID" + "    Weight" + "    Destination" + "    Price" + "   Type"));
		if(getAplist().size() != 0){
			for(int i=0; i<getAplist().size(); i++){
				item = item + 1;
				output.add(item + "            " + getAplist().get(i).getId() + "           " + getAplist().get(i).getWeight() + "            " + getAplist().get(i).getDestination() + "               " + getAplist().get(i).getPrice() + "          Air Parcel");
				total = total + getAplist().get(i).getPrice();
			}
		}
		
		if(getGclist().size() != 0){
			for(int i=0; i<getGclist().size(); i++){
				item = item + 1;
				output.add(item + "            " + getGclist().get(i).getId() + "           " + getGclist().get(i).getWeight() + "            " + getGclist().get(i).getDestination() + "               " + getGclist().get(i).getPrice() + "          Greeting Card");
				total = total + getGclist().get(i).getPrice();
			}
		}
		
		if(getSplist().size() != 0){
			for(int i=0; i<getSplist().size(); i++){
				item = item + 1;
				output.add(item + "            " + getSplist().get(i).getId() + "           " + getSplist().get(i).getWeight() + "            " + getSplist().get(i).getDestination() + "               " + getSplist().get(i).getPrice() + "          Sea Parcel");
				total = total + getSplist().get(i).getPrice();
			}
		}
		
		if(getLalist().size() != 0){
			for(int i=0; i<getLalist().size(); i++){
				item = item + 1;
				output.add(item +"            " + getLalist().get(i).getId() + "           " + getLalist().get(i).getWeight() + "            " + getLalist().get(i).getDestination() + "               " + getLalist().get(i).getPrice() + "          Letters");
				total = total + getLalist().get(i).getPrice();
			}
		}
		
		ShoppingCartGUI scg = new ShoppingCartGUI("Shopping Cart", "Preview of your current Shopping Cart", output, total);
		do{}while(scg.getInput() == false);
	
	}
	
	
	/**
	 * Method to delete and item from the shopping cart
	 */
	public void deleteItem(){
		int id = Integer.parseInt(JOptionPane.showInputDialog(null, "Enter the ID of the item that you want to delete"));
		boolean deleted = false;
		int length = String.valueOf(id).length();
		boolean confirm = false;
		String input = "";
		
		
		if(length == 2){
			input = JOptionPane.showInputDialog("Are you sure, you want to delete this item(y/n)?");
			if(input.equalsIgnoreCase("y")){
				for(int i=0; i<getAplist().size(); i++){
					if(id == getAplist().get(i).getId()){
						getAplist().remove(i);
						deleted = true;
						System.out.println("deleted successfully");
					}
				}
			}else{}
		}else if(length == 3){
			input = JOptionPane.showInputDialog("Are you sure, you want to delete this item(y/n)?");
			if(input.equalsIgnoreCase("y")){
				for(int i=0; i<getSplist().size(); i++){
					if(id == getSplist().get(i).getId()){
						getSplist().remove(i);
						deleted = true;
						System.out.println("deleted successfully");
					}
				}
			}else{}
		}
		
	}
	
	/**
	 * Method to access AirParcel List
	 * 
	 * @return AirParcel list
	 */
	public ArrayList <AirParcel> getAplist(){
		return apList;
	}
	
	/**
	 * Method to access GreetingCard List
	 * 
	 * @return GreetingCard list
	 */
	public ArrayList <GreetingCard> getGclist(){
		return gcList;
	}
	
	/**
	 * Method to access LetterAir List
	 * 
	 * @return LetterAir list
	 */
	public ArrayList <LetterAir> getLalist(){
		return laList;
	}
	
	/**
	 * Method to access AirParcel List
	 * 
	 * @return SeaParcel list
	 */
	public ArrayList <SeaParcel> getSplist(){
		return spList;
	}
	
	/**
	 * Method that show the checkout screen
	 * @exception for runnable checkout gui
	 */
	public void checkout(){
			EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					CheckoutGUI cg = new CheckoutGUI(frame);
					cg.getInput();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	
	
		long startTime = System.currentTimeMillis();
		while ((System.currentTimeMillis()-startTime)< 20*1000){
		}
		
		frame.setVisible(false);
	
		MessageDialog timeovermd = new MessageDialog("Stamping Machine", "Time Over For This Checkout, Please Try Again!");
		do{}while(timeovermd.getInput() == false);
		
	}
	
	
}