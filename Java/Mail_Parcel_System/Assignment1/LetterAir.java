package Assignment1;
import java.io.*;
import java.util.*;

import javax.swing.*;

/**
 * The class representing the physical object of Letters sub-class of International Post Item
 * @author Ravindu
 * @version 1.0 10/10/2014
 */

public class LetterAir extends InternationalPostItem{
	private String destination;
	private double weight;
	private double price;
	private String zone = "";
	private ArrayList<String> priceList;
	private ArrayList<String> destinationList;
	private final String type = "Letter Air";
	int parcelid = (int)(Math.random()*10000);
	
	/**
	 * Empty constructor of the Letter Air class which initializes the values to 0 and/or "" and calls the super class object
	 */
	public LetterAir(){
		super();
		destination = "";
		weight = 0;
		price = 0;
		
	}
	
	/**
	 * 
	 * Overloaded constructor which gets 2 parameters to initialize the variables and Calculates the price afterwards using calclaPrice method
	 * 
	 * @param weight - Weight of the Letter Air
	 * @param destination - Destination to which the Letter Air has to be sent
	 */
	public LetterAir(double weight, String destination) {
		super();
		this.destination = destination;
		this.weight = weight;
		this.price = 0 ;
		calclaPrice();
	}
	
	/**
	 * Method to set the destination of the Letters
	 *
	 * @param destination - Destination for the Letter Air that user input
	 */
	public void setDestination(String destination){
		this.destination = destination;
	}
	
	/**
	 * Sets the weight of the Letter
	 * 
	 * @param weight - The weight of the Letter
	 */
	public void setWeight(double weight){
		this.weight = weight;
	}
	
	/**
	 * Method to return each Letter Air's unique ID
	 * 
	 * @return unique ID identify Letter Air
	 */
	public int getId(){
		return parcelid;
	}

	/**
	 * Sets the price of the Letter Air
	 * 
	 * @param price - The price of the Letter Air
	 */
	public void setPrice(double price){
		this.price = price;
	}

	/**
	 * Method used to get the value of destination of the Letter Air
	 * 
	 * @return Destination of the Letter Air
	 */
	public String getDestination(){
		return this.destination;
	}
	
	/**
	 * Method used to get the value of weight of the Letter Air
	 * 
	 * @return The weight of the Letter Air
	 */
	public double getWeight(){
		return this.weight;
	}

	/**
	 * Method used to get the price of the Letter Air
	 * 
	 * @return The price of the Letter Air
	 */
	public double getPrice(){
		return this.price;
	}

	/**
	 * Method used to calculate the price of the Letter Air
	 */
	public void calclaPrice(){
		this.priceList = super.loadList("PriceMail.txt");
		this.destinationList = super.loadList("Destination.txt");
		double weighting = weight / 1000;
		zone = super.searchZone(destinationList, destination);
		price = super.searchlaPrice(priceList, zone, weighting);

	}
	
	/**
	 * Method used to printout the details of Letter Air to a file
	 * 
	 * @param out - output file stream
	 */
	public void printDetails(PrintWriter out){
		if (type != "" && destination != "" && weight != 0 && price != 0){
			super.write(out, type, destination, weight, price);
			JOptionPane.showMessageDialog(null, "Letters you selected/purchased have been written to the Invoice/Quotation successfully", "Notice", JOptionPane.INFORMATION_MESSAGE);
		}else{
			JOptionPane.showMessageDialog(null, "Error writing Letters information to the Invoice/Quotation", "Error", JOptionPane.ERROR_MESSAGE);
		}
	}
}