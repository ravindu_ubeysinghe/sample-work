package Assignment1;

import java.util.ArrayList;

import javax.swing.JOptionPane;

/**
 * The class used to validate inputs given by the user 
 * @author Ravindu
 * @version 1.0 10/10/2014
 */



public class Validate extends InternationalPostItem {
	private ArrayList<String> priceList = loadList("PriceParcelAir.txt");
	private ArrayList<String> destinationList = loadList("Destination.txt");
	private ArrayList<String> weightList = loadList("WeightParcelAir.txt");
	private ArrayList<String> gcprice = loadList("PriceGreetingCard.txt");
	
	/**
	 * Method to validate the destination entered by the user
	 * 
	 * @param - country - the country entered by the user
	 * @return a boolean  about the validity of the destination
	 */
	public boolean validDestination(String country){
		boolean found = false;
		
		for(int i=0; i<destinationList.size(); i++){
			if(destinationList.get(i).equalsIgnoreCase(country)){
				found = true;
				break;
			}
		}
		
		return found;
	}
	
	/**
	 * Method to validate the weight entered by the user for a greeting card item
	 * 
	 * @param - weight - the weight entered by the user
	 * @return a boolean  about the validity of the weight
	 */                                                                                                                                                                                                                                                                   
	public boolean validGCWeight(double weight){
		boolean valid = false;
		double maxWeight = 0;
		
		maxWeight = maxWeight(gcprice);
		
		if (weight/1000 <= maxWeight){
			valid = true;
		}else{
			valid = false;
		}
		
		return valid;
	}
	
	/**
	 * Method to validate the weight entered by the user
	 * 
	 * @param - weight - the weight entered by the user
	 * @return a boolean  about the validity of the weight
	 */ 
	public boolean validWeight(double weight, String country){
		boolean valid = false;
		int maxWeight = 0;
		
		maxWeight = searchMaxWeight(weightList, country);
		
		if (weight/1000 <= maxWeight || validGCWeight(weight) == true){
			valid = true;
		}else{
			valid = false;
		}
		
		return valid;
	}
	
	/**
	 * Method to validate the month entered by the user
	 * 
	 * @author Chris
	 * @param - month - A string containing the month entered by the user
	 * @return a boolean  about the validity of the month
	 */ 
	public boolean validMonth(String month){
		String MonthArray[] =  {"January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"};
		boolean correctMonth = false;
		for(int j=0; j<=MonthArray.length -1; j++){
			if(month.equalsIgnoreCase(MonthArray[j])){
				correctMonth = true;
			}
		}
		
		if(correctMonth){
			return true;
		}else{
			return false;
		}
		
	}
}
