package Assignment1;

import java.io.*;
import java.util.*;

import javax.swing.*;

/**
 * The class representing all the international post items(Super Class)
 * @author Ravindu, Chris, Stan
 * @version 1.0 10/10/2014
 */

public class InternationalPostItem {
	private String zone;
	
	public InternationalPostItem(){
		zone = "";
	}
	
	/**
	 * Method used to load all the lines of a text file in to an array list 
	 * 
	 * @param filename - name of the file that needs to be loaded.
	 * @exception FileNotFoundException for finding the existence of the file
	 * @exception IOException for reading the file
	 * @return An array list containing each line of the text file, the name of which was provided as the file name
	 */
	public ArrayList<String> loadList(String filename) {
		JFrame frame = new JFrame("ExceptionWindow");
		BufferedReader in = null;
		try {
			in = new BufferedReader(new FileReader(filename));
		} catch (FileNotFoundException e) {
			JOptionPane.showMessageDialog(frame, "File not found", "Error", JOptionPane.ERROR_MESSAGE);
			System.exit(0);
		}
		
		ArrayList<String> specificList = new ArrayList<String>();
		
		String temp = "";
		try{
			while((temp = in.readLine()) != null){
				specificList.add(temp);
			}
		}catch (IOException ioe){
			JOptionPane.showMessageDialog(null, "Error reading file", "Error", JOptionPane.ERROR_MESSAGE);
			System.exit(0);
		}
		//in.close();
		return specificList;
	}
	
	/**
	 * Method used to find the zone of a given country
	 * 
	 * @param destinationList - an array list containing all the destination information
	 * @param country - the country that the user entered
	 * @return The zone of the country as a string
	 */
	public String searchZone(ArrayList<String> destinationList, String country){
		boolean found = false;
		for(int i=0; i<destinationList.size(); i++){
			if(destinationList.get(i).equalsIgnoreCase("A")){
				zone = "A";
			}
			else if(destinationList.get(i).equalsIgnoreCase("B")){
				zone = "B";
			}
			else if(destinationList.get(i).equalsIgnoreCase("C")){
				zone = "C";
			}
			else if(destinationList.get(i).equalsIgnoreCase("D")){
				zone = "D";
			}
			
			if(destinationList.get(i).equalsIgnoreCase(country)){
				found = true;
				break;
			}
		}
		if(found){
			return zone;
		}else{
			return null;
		}
		
	}
	
	
	/**
	 * Method used to search the maximum weight for a certain Post Item
	 * 
	 * @param - weightList a list containing all the weighting information for a given item type
	 * @param - country - the country that the user input
	 * @exception - NumberFormatException for parsing the weight obtained from the file
	 * @return an integer containing the maximum weight for the item
	 */
	public int searchMaxWeight(ArrayList<String> weightList, String country){
		String tempArray[] = new String[2];
		int weight = 0;
		for(int i=0; i<weightList.size(); i++){
			if(weightList.get(i).contains(country)){
				tempArray = weightList.get(i).split("\\s+");
				try{
					weight = Integer.parseInt(tempArray[1]);
				}catch(NumberFormatException e){}
			}
		}
		return weight;
	}
	
	/**
	 * Method used to extract the price for AirParcel based on the weight and zone from the pricing information file
	 * 
	 * @param - priceList - a list containing all the pricing information for a given item type
	 * @param - zone - the zone of the country that the user input
	 * @param - weight - the weight that the user input for the item
	 * @param - maxWeight - the maxWeight for the item which was obtained from searchMaxWeight function
	 * @exception - NumberFormatException for parsing the input obtained from the file
	 * @return a double containing the price for the specific item
	 */
	public Double searchPrice(ArrayList<String> priceList, String zone, double weight, double maxWeight){
		String tempStr[] = new String[2];
		double price = 0;
		double extraCharge = 0;
		double maxCharge = 0;
		for(int i=0; i<priceList.size(); i++){
			if(priceList.get(i).equalsIgnoreCase(zone)){
				for(int j=i+1; j<=i+9; j++){ //Searching from this point
					//System.out.println(priceList.get(j));
					tempStr = priceList.get(j).split("\t");
					if(j == i+8){ //Getting the maximum specified price
						try{
							maxCharge = Double.parseDouble(tempStr[1]);
							//System.out.println("Max: " + maxCharge);
							
						}catch(NumberFormatException e){}
					}
					
					if(j == i+9){ //Getting the extra rate
						try{
							extraCharge = Double.parseDouble(tempStr[1]);
							//System.out.println("Extra: " + extraCharge);
							
						}catch(NumberFormatException e){}
					}else{
						try{ 
							if(weight > (Double.parseDouble(tempStr[0]) - 0.250) && weight <= Double.parseDouble(tempStr[0])){
								price = Double.parseDouble(tempStr[1]); // Error: converting A to a float here 
							}
						}catch(NumberFormatException e){}
					}
				}
				if (weight > 2){
					price = maxCharge + (((weight - 2)*2) * extraCharge);
				}
				break;
			}
		}
		
		return price;
	}
	
	/**
	 * Method used to find the zone of a given country
	 * 
	 * @param destinationList - an array list containing all the destination information
	 * @param country - the country that the user entered
	 * @return The zone of the country as a string
	 */
	public String searchSpZone(ArrayList<String> destinationList, String country){
		boolean found = false;
		for(int i=0; i<destinationList.size(); i++){
			if(destinationList.get(i).equalsIgnoreCase("D")){
				zone = "C";
			}
			else if(destinationList.get(i).equalsIgnoreCase("C")){
				zone = "D";
			}
			if(destinationList.get(i).equalsIgnoreCase(country)){
				found = true;
				break;
			}
		}
		if(found){
			return zone;
		}else{
			return null;
		}
		
	}
	
	/**
	 * Method used to search the maximum weight for a certain Post Item (duplication of searchMaxWeight)
	 * 
	 * @param - weightList a list containing all the weighting information for a given item type
	 * @param - country - the country that the user input
	 * @exception - NumberFormatException for parsing the weight obtained from the file
	 * @return an integer containing the maximum weight for the item
	 */
	public int searchSpMaxWeight(ArrayList<String> weightList, String country){
		String tempArray[] = new String[2];
		int weight = 0;
		for(int i=0; i<weightList.size(); i++){
			if(weightList.get(i).contains(country)){
				tempArray = weightList.get(i).split("\\s+");
				try{
					weight = Integer.parseInt(tempArray[1]);
				}catch(NumberFormatException e){}
			}
		}
		return weight;
	}
	
	/**
	 * Method used to extract the price for SeaParcel based on the weight and zone from the pricing information file(a duplicate of searchPrice function)
	 * 
	 * @param - priceList - a list containing all the pricing information for a given item type
	 * @param - zone - the zone of the country that the user input
	 * @param - weight - the weight that the user input for the item
	 * @param - maxWeight - the maxWeight for the item which was obtained from searchMaxWeight function
	 * @exception NumberFormatException for parsing the maxCharge and extraCharge and input obtained from the file
	 * @return a double containing the price for the specific item
	 */
	public Double searchSpPrice(ArrayList<String> priceList, String zone, double weight, double maxWeight){
		String tempStr[] = new String[2];
		double price = 0;
		double extraCharge = 0;
		double maxCharge = 0;
		for(int i=0; i<priceList.size(); i++){
			if(priceList.get(i).equalsIgnoreCase(zone)){
				for(int j=i+1; j<=i+9; j++){ //Searching from this point
					//System.out.println(priceList.get(j));
					tempStr = priceList.get(j).split("\t");
					if(j == i+8){ //Getting the maximum specified price
						try{
							maxCharge = Double.parseDouble(tempStr[1]);
							//System.out.println("Max: " + maxCharge);
							
						}catch(NumberFormatException e){}
					}
					
					if(j == i+9){ //Getting the extra rate
						try{
							extraCharge = Double.parseDouble(tempStr[1]);
							//System.out.println("Extra: " + extraCharge);
							
						}catch(NumberFormatException e){}
					}else{
						try{ 
							if(weight > (Double.parseDouble(tempStr[0]) - 0.250) && weight <= Double.parseDouble(tempStr[0])){
								price = Double.parseDouble(tempStr[1]); // Error: converting A to a float here 
							}
						}catch(NumberFormatException e){}
					}
				}
				if (weight > 2){
					price = maxCharge + (((weight - 2)*2) * extraCharge);
				}
				break;
			}
		}
		
		return price;
	}
	

	/**
	 * Method used to extract the price for GreetingCard based on the weight and zone from the pricing information file
	 * 
	 * @param - gcprice - a list containing all the pricing information for a given item type
	 * @exception - NumberFormatException for parsing the weight obtained from the file
	 * @return a double containing the price for the specific item
	 */
	public double searchGCPrice(ArrayList<String> gcprice){
		String temp[] = new String[2];
		double price = 0;
		
		temp = gcprice.get(0).split("\t");
		try{
		price = Double.parseDouble(temp[1]);
		}catch(NumberFormatException e){}
		
		return price;
	}
	
	/**
	 * Method used to search the Maximum Weight for Greeting Cards
	 * 
	 * @author Chris
	 * @param - gcprice - a list containing all the pricing information for a given item type
	 * @exception - NumberFormatException for parsing the weight obtained from the file
	 * @return a double containing the maximum weight for the given greeting card
	 */
	public double maxWeight(ArrayList<String> gcprice){
		String temp2[] = new String[2];
		double weight = 0;
		
		temp2 = gcprice.get(1).split("\t");
		try{
		weight = Double.parseDouble(temp2[1]);
		}catch(NumberFormatException e){}
		return weight;
	}
	
	/**
	 * Method used to search the Price for Greeting Cards
	 * 
	 * @author Chris
	 * @param - gcsprice - a list containing all the pricing information for a given item type
	 * @exception - NumberFormatException for parsing the weight obtained from the file
	 * @return a double containing the price for the given greeting card
	 */
	public double searchGCSPrice(ArrayList<String> gcsprice){
		String temp1[] = new String[2];
		double price = 0;
		
		temp1 = gcsprice.get(0).split("\t");
		try{
		price = Double.parseDouble(temp1[1]);
		}catch(NumberFormatException e){}
		

		return price;
	}
	
	/**
	 * Method used to find the zone of a given country
	 * 
	 * @param destinationList - an array list containing all the destination information
	 * @param country - the country that the user entered
	 * @return The zone of the country as a string
	 */
	public String searchlaZone(ArrayList<String> destinationList, String country){
		boolean found = false;
		for(int i=0; i<destinationList.size(); i++){
			if(destinationList.get(i).equalsIgnoreCase("A")){
				zone = "A";
			}
			else if(destinationList.get(i).equalsIgnoreCase("B")){
				zone = "B";
			}
			else if(destinationList.get(i).equalsIgnoreCase("C")){
				zone = "C";
			}
			else if(destinationList.get(i).equalsIgnoreCase("D")){
				zone = "D";
			}
			
			if(destinationList.get(i).equalsIgnoreCase(country)){
				found = true;
				break;
			}
		}
		if(found){
			return zone;
		}else{
			return null;
		}
		
	}
	
	/**
	 * Method used to extract the price for SeaParcel based on the weight and zone from the pricing information file(a duplicate of searchPrice function)
	 * 
	 * @param - priceList - a list containing all the pricing information for a given item type
	 * @param - zone - the zone of the country that the user input
	 * @param - weight - the weight that the user input for the item
	 * @return a double containing the price for the specific item
	 */
	public Double searchlaPrice(ArrayList<String> priceList, String zone, double weight){
		String tempStr[] = new String[2];
		double price=0;
		for(int i=0; i<priceList.size();i++){
		if(priceList.get(i).equalsIgnoreCase(zone)){
		for(int j=i+1;j<=i+4;j++){
		    tempStr = priceList.get(j).split("\t");
			
			try{ 
				if(weight >= Double.parseDouble(tempStr[0]) ){
					price = Double.parseDouble(tempStr[1]); // Error: converting A to a float here 
				}
			}catch(NumberFormatException e){}
		}
		
		break;
		}
		}
		return price;
		}
	
	/**
	 * Method used to printout item details to the invoice(duplicate of the write method)
	 * 
	 * @param out - the print writer object passed from the main method
	 * @param type - type of the parcel
	 * @param destination - the destination user input
	 * @param weight - the weight for the item that the user entered
	 * @param price - price of the item obtained from the functions
	 */
	public void writela(PrintWriter out, String type, String destination, double weight, double price){
			out.println("Type: " + type); 
			out.println("Destination: " + destination);
			out.println("Weight: " + (weight) + "g");
			out.println("Price: " + price);
			out.println("");
			
	}
	
	/**
	 * Method used to printout item details to the invoice
	 * 
	 * @param out - the print writer object passed from the main method
	 * @param type - type of the parcel
	 * @param destination - the destination user input
	 * @param weight - the weight for the item that the user entered
	 * @param price - price of the item obtained from the functions
	 */
	public void write(PrintWriter out, String type, String destination, double weight, double price){
			//PrintWriter out = new PrintWriter(new FileWriter("StampingMachineOutput.txt"));
			out.println("Type: " + type); 
			out.println("Destination: " + destination);
			out.println("Weight: " + (weight) + "g");
			out.println("Price: " + price);
			out.println("");
	}
	
	/**
	 * Method used to printout item details to the invoice(duplicate of the write method)
	 * 
	 * @param out - the print writer object passed from the main method
	 * @param type - type of the parcel
	 * @param destination - the destination user input
	 * @param weight - the weight for the item that the user entered
	 * @param price - price of the item obtained from the functions
	 */
	public void writegc(PrintWriter out, String type, String seasson, double weight, double price){
		//PrintWriter out = new PrintWriter(new FileWriter("StampingMachineOutput.txt"));
		out.println("Type: " + type); 
		out.println("Nov/Dec: " + seasson );
		out.println("Weight:" +   weight +"g");
		out.println("Price: " + price);
		out.println("");
}
	/**
	 * Method used to printout item details to the invoice(duplicate of the write method)
	 * 
	 * @param out - the print writer object passed from the main method
	 * @param type - type of the parcel
	 * @param destination - the destination user input
	 * @param weight - the weight for the item that the user entered
	 * @param price - price of the item obtained from the functions
	 */
	public void writesp(PrintWriter out, String type, String destination, double weight, double price){
		//PrintWriter out = new PrintWriter(new FileWriter("StampingMachineOutput.txt"));
		out.println("Type: " + type); 
		out.println("Destination: " + destination);
		out.println("Weight: " + (weight) + "g");
		out.println("Price: " + price);
		out.println("");
}


}


