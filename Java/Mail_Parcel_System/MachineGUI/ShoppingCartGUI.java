package MachineGUI;

import java.awt.*;
import java.awt.event.*;
import java.util.ArrayList;

import javax.swing.*;
import javax.swing.border.EmptyBorder;

/**
 * The GUI class containing the interface for the shopping cart dialog which shows all the items in the shopping cart and the total
 * @author Ravindu
 * @version 1.0 10/10/2014
 */

public class ShoppingCartGUI extends JDialog {
	private ArrayList<JLabel> labelList = new ArrayList<JLabel>();
	private JPanel jp = new JPanel();
	private JButton okButton = new JButton("OK");
	private boolean input = false;
	
	/**
	 * The constructor of the ShoppingCartGUI class which initializes the values for JDialog, the super class object and other 
	 * GUI components that are used in this class
	 */
	public ShoppingCartGUI(String title, String label, ArrayList<String> stringList, double total){
		setTitle(title);
		setVisible(true);
		setBounds(100, 100, 450, 300);
		getContentPane().setLayout(new BorderLayout());
		jp.setLayout(new FlowLayout());
		jp.setBorder(new EmptyBorder(5, 5, 5, 5));
		
		for(int i=0; i<stringList.size(); i++){
			labelList.add(new JLabel(stringList.get(i)));
		}
		
		String labelText = "Total: " + total;
		JLabel totalLabel = new JLabel(labelText);
		
		getContentPane().add(jp, BorderLayout.CENTER);
		{
			for(int i =0; i<labelList.size(); i++){
				if(i==0){
					labelList.get(i).setFont(new Font("Franklin Gothic Demi", Font.PLAIN, 17));
					jp.add(labelList.get(i));
				}
				jp.add(labelList.get(i));
				
			}
			
			//jp.add(totalLabel);
		}
		
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				buttonPane.add(totalLabel);
			}
			{
				okButton.setActionCommand("OK");
				buttonPane.add(okButton);
				getRootPane().setDefaultButton(okButton);
			}
			
		}
		
		
	}
	
	/**
	 * Method to return the value of the boolean called input which is set true once the user has pressed the 'OK' button
	 * 
	 * @return input - boolean containing true or false based on whether or not the user has clicked the 'OK' button
	 */
	public boolean getInput(){
		do{
			okButton.addActionListener(new ActionListener(){
				public void actionPerformed(ActionEvent e){
					ShoppingCartGUI.this.input = true;
					setVisible(false);
				}
			});
		}while(input == false );
		return input;
	}
}
