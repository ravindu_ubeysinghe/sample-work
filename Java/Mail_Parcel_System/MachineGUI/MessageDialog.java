package MachineGUI;

import java.awt.*;
import java.awt.event.*;

import javax.swing.*;

/**
 * The GUI class containing the interface for the message dialog which takes two parameters from the user and displays a message
 * @author Ravindu
 * @version 1.0 10/10/2014
 */

public class MessageDialog extends JFrame {
	private JPanel jp = new JPanel(new GridBagLayout());
	private JLabel jl = new JLabel("", JLabel.CENTER);
	private JButton jb = new JButton("OK");
	private String input;
	private boolean buttonClicked  = false;
	
	/**
	 * The constructor of the MessageDialog class which initializes the values for JFrame, the super class object and other 
	 * GUI components that are used in this class
	 */
	public MessageDialog(String title, String label){
		setTitle(title);
		setVisible(true);
		setSize(400,150);
		setLocationRelativeTo(null);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		
		/*jt.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				input = jt.getText();
			}
		});*/
		GridBagConstraints c = new GridBagConstraints();
		jl.setText(label);
		jl.setFont(new Font("Serif", Font.PLAIN, 20));
		c.gridx = 0;
		c.gridy = 0;
		c.insets = new Insets(10,10,10,10);
		jp.add(jl, c);
		
		c.gridx = 0;
		c.gridy = 2;
		c.insets = new Insets(10,10,10,10);
		jb.setSize(new Dimension(60,30));
		jp.add(jb, c);
		
		/*jb.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				setVisible(false);
			}
		});*/
		
		add(jp);
		pack();
	}
	
	/**
	 * Method to return the value of the boolean called input which is set true once the user has pressed the 'OK' button
	 * 
	 * @return input - boolean containing true or false based on whether or not the user has clicked the 'OK' button
	 */
	public boolean getInput(){
		while(buttonClicked == false){
			
			jb.addActionListener(new ActionListener(){
				public void actionPerformed(ActionEvent e){
					setVisible(false);
					buttonClicked = true;
				}
			});
		}
		
		return buttonClicked;
	}
	
	
	/*public static void main(String [] args){
		MessageDialog sm = new MessageDialog("Stamping Machine", "Sample Message, Click Ok to Continue ");
		do{}while(sm.getInput() == false);
		System.out.println("fsfgsfsf");
	}*/
}