package MachineGUI;

import java.awt.*;
import java.awt.event.*;
import java.util.ArrayList;

import javax.swing.*;

/**
 * The GUI class containing the interface for the option dialog which takes an array of inputs from the user and displays it
 * in order to get the user selection
 * @author Ravindu
 * @version 1.0 10/10/2014
 */

public class OptionDialog extends JFrame {
	private JPanel jp = new JPanel(new GridBagLayout());
	private JPanel jp1 = new JPanel(new GridBagLayout());
	private ArrayList<JButton> buttonList = new ArrayList<JButton>();
	private JLabel jl = new JLabel();
	private String input;
	private int button;
	
	/**
	 * The constructor of the OptionDialog class which initializes the values for JFrame, the super class object and other 
	 * GUI components that are used in this class
	 */
	public OptionDialog(String title, String label, String[] options){
		setTitle(title);
		setVisible(true);
		setSize(600,150);
		setLocationRelativeTo(null);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setLayout(new GridLayout(2,1));
		
		
		jl.setFont(new Font("Serif", Font.PLAIN, 20));
		jl.setText(label);
		jp1.add(jl);
		
		GridBagConstraints c = new GridBagConstraints();	
		
		for(int i=0; i<options.length; i++){
			buttonList.add(new JButton(options[i]));
		}
		
		for(int i=0; i<buttonList.size(); i++){
			c.gridx = i;
			c.gridy = 2;
			c.insets = new Insets(10,10,10,10);
			jp.add(buttonList.get(i), c);
		}
				
		
		/*jt.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				input = jt.getText();
			}
		});*/
		
		
		/*jb.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				OptionDialog.this.input = jt.getText();
				setVisible(false);
			}
		});*/
		
		add(jp1);
		add(jp);
		pack();
	}
	
	/**
	 * Method to return the value of the boolean called input which is set true once the user has pressed any of option buttons
	 * 
	 * @return input - boolean containing true or false based on whether or not the user has clicked any of option buttons
	 */
	public String getInput(){
		do{
			for(int i=0; i<buttonList.size(); i++){
				buttonList.get(i).addActionListener(new ActionListener(){
					public void actionPerformed(ActionEvent e){
						JButton newButton = (JButton)e.getSource();
						input = newButton.getText();
						setVisible(false);
					}
				});
			}
		}while(input == null);
		
		return input;
	}
	
	/**
	 * Method to return the value of the integer called button which is set to the index number of the option that's clicked by the user
	 * 
	 * @return button - Integer containing the index of the option button that's clicked.
	 */
	public int getButton(){
		for(int i=0; i<buttonList.size(); i++){
			if(buttonList.get(i).getText() == input){
				button = i;
			}
		}
		return button;
	}
	
	public static void main(String [] args){
		String[] options = {"Air Parcel", "Sea Parcel", "Greeting Card", "Letter Air"};
		OptionDialog sm = new OptionDialog("Stamping Machine", "Enter your name: ", options);
		do{}while(sm.getInput() == null);
		System.out.println(sm.getInput());
		System.out.println(sm.getButton());
		
	}
}
