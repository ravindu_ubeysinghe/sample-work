package MachineGUI;

import java.awt.*;
import java.awt.event.*;

import javax.swing.*;

/**
 * The GUI class containing the interface for the input dialog which takes input from the user and returning it to the main method
 * @author Ravindu
 * @version 1.0 10/10/2014
 */

public class InputDialog extends JFrame {
	private JLabel jl1 = new JLabel();
	private JPanel jp = new JPanel();
	private JLabel jl = new JLabel();
	private JTextField jt = new JTextField(30);
	private JButton jb = new JButton("Enter");
	private String input;
	
	/**
	 * The constructor of the InputDialog class which initializes the values for JFrame, the super class object and other 
	 * GUI components that are used in this class
	 */
	public InputDialog(String title, String label){
		setTitle(title);
		setVisible(true);
		setSize(400,150);
		setLocationRelativeTo(null);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		jl.setFont(new Font("Serif", Font.PLAIN, 20));
		jl1.setFont(new Font("Serif", Font.PLAIN, 20));
		jp.add(jl1);
		jp.add(jt);
		jl1.setText(label);
		
		/*jt.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				input = jt.getText();
			}
		});*/
		
		jp.add(jb);
		
		/*jb.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				InputDialog.this.input = jt.getText();
				setVisible(false);
			}
		});*/
		
		jp.add(jl);
		
		add(jp);
	}
	
	public String getInput(){
		while(input == null ){
			jb.addActionListener(new ActionListener(){
				public void actionPerformed(ActionEvent e){
					InputDialog.this.input = jt.getText();
					setVisible(false);
				}
			});
		}
		return input;
	}
	
	/**
	 * Method to return the input entered by the user to the main method as a string
	 * 
	 * @return input - A string containing the input that the user entered
	 */
	public static void main(String [] args){
		InputDialog sm = new InputDialog("Stamping Machine", "Enter your name: ");
		do{}while(sm.getInput() == null);
		System.out.println("sdfvksfmn;vk");
		
	}
}
