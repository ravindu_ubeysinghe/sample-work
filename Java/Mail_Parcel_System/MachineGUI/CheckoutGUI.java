package MachineGUI;

import java.awt.*;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.text.JTextComponent;

import java.awt.event.*;

/**
 * The GUI class containing the interface for the checkout function in Stamping Machine
 * @author Ravindu
 * @version 1.0 10/10/2014
 */

public class CheckoutGUI {

	private JFrame frame;
	private JPanel contentPane;
	private JTextField cardno;
	private JLabel expdatelbl;
	private JTextField cardholder;
	private JLabel billingaddrlbl;
	private JLabel cardholderlbl;
	private JTextField expdate;
	private JTextField billingaddr;
	private final JPanel panel = new JPanel();
	private JLabel lblComments;
	private JTextArea textArea;
	private JButton btnOk;
	private JPanel panel_1;
	private String cardnoStr ="", addressStr = "", expiryDateStr = "", holderNameStr ="";
	private int cardnoInt = 0, expiryInt = 0;
	private boolean result = false;
	private boolean success = false;

	/**
	 * Launch the application.
	 */
	/*public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					CheckoutGUI frame = new CheckoutGUI();
					frame.getInput();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}*/

	/**
	 * The constructor of the CheckoutGUI class which initializes the values for JFrame, the super class object and other 
	 * GUI components that are used in this class
	 */
	public CheckoutGUI(JFrame framein) {
		this.frame = framein;
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setBounds(100, 100, 450, 400);
		frame.setVisible(true);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new GridLayout(11,1));
		frame.setContentPane(contentPane);
		
		JLabel cardnolbl = new JLabel("Credit Card Number:");
		cardnolbl.setFont(new Font("Franklin Gothic Demi", Font.PLAIN, 17));
		contentPane.add(cardnolbl);
		
		cardno = new JTextField();
		cardno.setSelectionColor(new Color(105, 105, 105));
		cardno.setSelectedTextColor(new Color(255, 255, 255));
		cardno.setPreferredSize(new Dimension(8, 20));
		//cardno.setMargin(new Insets(12, 12, 12, 12));
		cardno.setDisabledTextColor(new Color(105, 105, 105));
		cardno.setToolTipText("Card Card Number Goes Here!");
		cardno.setBackground(new Color(240, 248, 255));
		cardno.setFont(new Font("DialogInput", Font.PLAIN, 14));
		cardno.setColumns(2);
		
		cardno.addFocusListener(new FocusListener() {

			@Override
			public void focusLost(FocusEvent fl) {
				final JTextComponent c = (JTextComponent) fl.getSource();
			    cardnoStr = c.getText();
			    try{
			    	cardnoInt = Integer.parseInt(cardnoStr);
			    }catch(NumberFormatException e){
			    	cardno.setText("Please enter an integer as the Card No");
			    }
			   	if (cardnoStr.equalsIgnoreCase("")){
			   		cardno.setText("Error, The Card Number Should Not Be Empty");
			   	}
				
			}
			
			@Override
			public void focusGained(FocusEvent fe) {
				cardno.setText("");
				
			}
			});
		contentPane.add(cardno);
		
		expdatelbl = new JLabel("Expiration Date:");
		expdatelbl.setFont(new Font("Franklin Gothic Demi", Font.PLAIN, 17));
		contentPane.add(expdatelbl);
		
		expdate = new JTextField();
		expdate.setToolTipText("Expiration Date of the Credit Card Goes Here!");
		expdate.setSelectionColor(SystemColor.controlDkShadow);
		expdate.setSelectedTextColor(Color.WHITE);
		expdate.setPreferredSize(new Dimension(8, 20));
		//expdate.setMargin(new Insets(12, 12, 12, 12));
		expdate.setFont(new Font("DialogInput", Font.PLAIN, 14));
		expdate.setDisabledTextColor(SystemColor.controlDkShadow);
		expdate.setColumns(2);
		expdate.setBackground(new Color(240, 248, 255));
		
		expdate.addFocusListener(new FocusListener() {

			@Override
			public void focusLost(FocusEvent fl) {
				final JTextComponent c = (JTextComponent) fl.getSource();
			    expiryDateStr = c.getText();
			    try{
			    	expiryInt = Integer.parseInt(expiryDateStr);
			    }catch(NumberFormatException e){
			    	expdate.setText("Please enter an integer as the Expiration Date");
			    }
			   	if (expiryDateStr.equalsIgnoreCase("")){
			   		expdate.setText("Error, The Expiration Date Should Not Be Empty");
			   	}
				
			}
			
			@Override
			public void focusGained(FocusEvent fe) {
				expdate.setText("");
				
			}
			});
		
		contentPane.add(expdate);
		
		billingaddrlbl= new JLabel("Billing Address:");
		billingaddrlbl.setFont(new Font("Franklin Gothic Demi", Font.PLAIN, 17));
		contentPane.add(billingaddrlbl);
		
		billingaddr = new JTextField();
		billingaddr.setToolTipText("Billing Address Goes Here!");
		billingaddr.setSelectionColor(SystemColor.controlDkShadow);
		billingaddr.setSelectedTextColor(Color.WHITE);
		billingaddr.setPreferredSize(new Dimension(8, 20));
		//billingaddr.setMargin(new Insets(12, 12, 12, 12));
		billingaddr.setFont(new Font("DialogInput", Font.PLAIN, 14));
		billingaddr.setDisabledTextColor(SystemColor.controlDkShadow);
		billingaddr.setColumns(2);
		billingaddr.setBackground(new Color(240, 248, 255));
		
		billingaddr.addFocusListener(new FocusListener() {

			@Override
			public void focusLost(FocusEvent fl) {
				final JTextComponent c = (JTextComponent) fl.getSource();
			    addressStr = c.getText();
			   	if (addressStr.equalsIgnoreCase("")){
			   		billingaddr.setText("Error, The Billing Address Should Not Be Empty");
			   	}
				
			}
			
			@Override
			public void focusGained(FocusEvent fe) {
				billingaddr.setText("");
				
			}
			});
		
		contentPane.add(billingaddr);
		
		cardholderlbl = new JLabel("Enter Card Holder's Name:");
		cardholderlbl.setFont(new Font("Franklin Gothic Demi", Font.PLAIN, 17));
		contentPane.add(cardholderlbl);
		
		cardholder = new JTextField();
		cardholder.setToolTipText("Card Holder's Name Goes Here!");
		cardholder.setSelectionColor(SystemColor.controlDkShadow);
		cardholder.setSelectedTextColor(Color.WHITE);
		cardholder.setPreferredSize(new Dimension(8, 20));
		cardholder.setFont(new Font("DialogInput", Font.PLAIN, 14));
		cardholder.setDisabledTextColor(SystemColor.controlDkShadow);
		cardholder.setColumns(2);
		cardholder.setBackground(new Color(240, 248, 255));
		
		cardholder.addFocusListener(new FocusListener() {

			@Override
			public void focusLost(FocusEvent fl) {
				final JTextComponent c = (JTextComponent) fl.getSource();
			    holderNameStr = c.getText();
			   	if (holderNameStr.equalsIgnoreCase("")){
			   		cardholder.setText("Error, The Card Holder's Name Should Not Be Empty");
			   	}
				
			}
			
			@Override
			public void focusGained(FocusEvent fe) {
				cardholder.setText("");
				
			}
			});
		
		contentPane.add(cardholder);
		
		lblComments = new JLabel("Comments:");
		lblComments.setFont(new Font("Franklin Gothic Demi", Font.PLAIN, 17));
		contentPane.add(lblComments);
		
		textArea = new JTextArea();
		textArea.setBorder(null);
		textArea.setToolTipText("Card Holder's Name Goes Here!");
		textArea.setSelectionColor(SystemColor.controlDkShadow);
		textArea.setSelectedTextColor(Color.WHITE);
		textArea.setPreferredSize(new Dimension(8, 20));
		textArea.setFont(new Font("DialogInput", Font.PLAIN, 14));
		textArea.setDisabledTextColor(SystemColor.controlDkShadow);
		textArea.setColumns(2);
		textArea.setBackground(new Color(240, 248, 255));
		contentPane.add(textArea);
		
		
		contentPane.add(panel);
		panel.setLayout(new GridLayout(1,3));
		panel_1 = new JPanel();
		panel.add(panel_1);
		
		btnOk = new JButton("Pay Now");
		btnOk.setFont(new Font("Franklin Gothic Demi Cond", Font.PLAIN, 13));
		panel.add(btnOk);
		
		JPanel panel_2 = new JPanel();
		panel.add(panel_2);
		
		/*btnOk.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
					//setVisible(false);
					if(!(cardnoStr == "" || addressStr == "" || expiryDateStr == "" || holderNameStr == "")){
						result = false;
						JOptionPane.showMessageDialog(null, "Please make sure that all the fields are filled", "Error", JOptionPane.ERROR_MESSAGE);
					}else if(!(cardnoInt == 0) || (expiryInt == 0)){
						result = false;
						MessageDialog error = new MessageDialog("Error", "Card Number and The Expiration Date should be Integers");
						do{}while(error.getInput() == false);
					}else{
						System.out.println("um here");
						result = true;
						setVisible(false);
					}
					
			}
		});*/
	}
	
	/**
	 * Method to return the value of the boolean called input which is set true once the user has pressed the 'OK' button
	 * After validating the user entered details
	 * 
	 * @return input - boolean containing true or false based on whether or not the user has clicked the 'OK' button
	 */
	public boolean getInput(){
		btnOk.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
					
					if((cardnoStr == "" || addressStr == "" || expiryDateStr == "" || holderNameStr == "")){
						//result = false;
						JOptionPane.showMessageDialog(null, "Please make sure that all the fields are filled", "Error", JOptionPane.ERROR_MESSAGE);
						CheckoutGUI.this.success= false;
						cardno.requestFocus();
					}else if((cardnoInt == 0) || (expiryInt == 0)){
						//result = false;
						JOptionPane.showMessageDialog(null, "Card Number and The Expiration Date should be Integers", "Error", JOptionPane.ERROR_MESSAGE);
						CheckoutGUI.this.success= false;
						cardno.requestFocus();
					}else{
						JOptionPane.showMessageDialog(null, "Your Payment Has Been Successfully Processed, Thank you for using THE STAMPING MACHINE!", "Stammping Machine", JOptionPane.INFORMATION_MESSAGE);;
						//result = true;
						frame.setVisible(false);
						CheckoutGUI.this.success= true;
					}
					
			}
		});
		
		return this.success;

}
}
	

