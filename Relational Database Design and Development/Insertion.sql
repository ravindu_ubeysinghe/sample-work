USE FastAndTastyFoods;

BEGIN TRANSACTION;

INSERT INTO Pay (PayLevel,PayLevelName,HourlySalary,Supperannuation) 
            VALUES ('E','Manager','$40','12');
INSERT INTO Pay (PayLevel,PayLevelName,HourlySalary,Supperannuation) 
            VALUES ('B','Junior','$15','3');
INSERT INTO Pay (PayLevel,PayLevelName,HourlySalary,Supperannuation) 
            VALUES ('D','Assistant Manager','$30','8');
INSERT INTO Pay (PayLevel,PayLevelName,HourlySalary,Supperannuation) 
            VALUES ('A','Trainee','$12','2');
INSERT INTO Pay (PayLevel,PayLevelName,HourlySalary,Supperannuation) 
            VALUES ('C','Senior','$18','4');

Print '5 records have been inserted into Pay table'

COMMIT;
-----------------------------------------------------------------------------------

BEGIN TRANSACTION;

INSERT INTO Product (ProductId,ProductName,ProductPrice,ServingSize,ProductDescription,CaloriesPerServe,Fat,Protein,CarbohydrateContent) 
            VALUES ('CKHOTDG','HOT DOG','$1.80','Large', 'Hot Dog Large with Chicken Sausage', '242', '14.5g', '10.4g', '21.36g');
INSERT INTO Product (ProductId,ProductName,ProductPrice,ServingSize,ProductDescription,CaloriesPerServe,Fat,Protein,CarbohydrateContent) 
            VALUES ('CKBURGR','CHICKEN BURGER','$2.00','Medium', 'Chicken Burger Medium with or without Cheese', '140', '5.0g', '22.0g', '34.25g');
INSERT INTO Product (ProductId,ProductName,ProductPrice,ServingSize,ProductDescription,CaloriesPerServe,Fat,Protein,CarbohydrateContent) 
            VALUES ('FRNCHFR','FRENCH FRIES','$1.00','Medium', 'Medium Cryspy French Fries', '360', '22.82g', '5.04g', '50.29g');
INSERT INTO Product (ProductId,ProductName,ProductPrice,ServingSize,ProductDescription,CaloriesPerServe,Fat,Protein,MainVitamin,CarbohydrateContent) 
            VALUES ('CKSALAD','CHICKEN SALAD','$8.95','One Cup', 'Cryspy Chicken Classic Salad', '417', '31.50g', '29,48g', 'C', '2.55g');
INSERT INTO Product (ProductId,ProductName,ProductPrice,ServingSize,ProductDescription,CaloriesPerServe,Fat,Protein,SugarContent,FiberContent,CarbohydrateContent) 
            VALUES ('VGBRITO','VEGETABLE BURRITO','$5.25','Medium', 'Burrito With Rice, Beans and Vegetables', '226', '6.75g', '7.03g', '6.00g', '4.00g', '35.72g');

Print '5 records have been inserted into Product table'

COMMIT;
-----------------------------------------------------------------------------------

BEGIN TRANSACTION;

INSERT INTO Supplier (SupplierId,BusinessName,ABN,ProductId,ContactPersonName,ContactPersonPhone,Address) 
			VALUES ('SUP786','Cakewalk Ltd','54671098345','CKBURGR','Jemima','(61) 3221 3409','Ap #470-9332 Semper Rd.');
INSERT INTO Supplier (SupplierId,BusinessName,ABN,ProductId,ContactPersonName,ContactPersonPhone,Address) 
			VALUES ('SUP098','A1umka Food','56089731141','FRNCHFR','Amela','(61) 4812 5972','Ap #988-1490 Orci, Road');
INSERT INTO Supplier (SupplierId,BusinessName,ABN,ProductId,ContactPersonName,ContactPersonPhone,Address) 
			VALUES ('SUP394','FFS Foods','53708085619','VGBRITO','Xantha','(61) 0458 6737','P.O. Box 505, 2585 Congue, St.');
INSERT INTO Supplier (SupplierId,BusinessName,ABN,ProductId,ContactPersonName,ContactPersonPhone,Address) 
			VALUES ('SUP132','Uniraw Diaries','53004085616','CKHOTDG','Olympia','(61) 4239 0118','2085 Tellus. St.');
INSERT INTO Supplier (SupplierId,BusinessName,ABN,ProductId,ContactPersonName,ContactPersonPhone,Address) 
			VALUES ('SUP564','Cobizco Food','56710065342','CKSALAD','Chancellor','(61) 4887 3249','699-5777 Vestibulum Rd.');

Print '5 records have been inserted into Supplier table'

COMMIT;
-----------------------------------------------------------------------------------

BEGIN TRANSACTION;

INSERT INTO Store  (StoreId,StoreName,Location,Address,OpeningHours,ManagerId,ManagerName) 
			VALUES ('1','FAT Joondalup','Joondalup','Ap #506-4940 Donec Avenue','09:30 - 19:30','101','Sydnee Sosa');
INSERT INTO Store  (StoreId,StoreName,Location,Address,OpeningHours,ManagerId,ManagerName) 
			VALUES ('3','FAT Beldon','Beldon','No. 984, 7770 Aliquam Rd.','09:30 - 19:30','103','Iona Macintosh');
INSERT INTO Store  (StoreId,StoreName,Location,Address,OpeningHours,ManagerId,ManagerName) 
			VALUES ('2','FAT Wanneroo','Wanneroo','470 Hymenaeos. Road','09:30 - 19:30','102','Brenna Blackwell');
INSERT INTO Store  (StoreId,StoreName,Location,Address,OpeningHours,ManagerId,ManagerName) 
			VALUES ('5','FAT Kingsley','Kingsley','595-6926 Tellus Street','10:00 - 19:00','105','Nigel Carlson');
INSERT INTO Store  (StoreId,StoreName,Location,Address,OpeningHours,ManagerId,ManagerName) 
			VALUES ('4','FAT Ocean Keys','Ocean Keys','P.O. Box 793, 6809 Est, Road','08:00 - 21:00','104','Maite Daugherty');

Print '5 records have been inserted into Store table'

COMMIT;
-----------------------------------------------------------------------------------

BEGIN TRANSACTION;

INSERT INTO StoreProduct  (StoreId,ProductId,PurchasedOty,Month,Year) 
			VALUES ('1','CKHOTDG','250','May','2013');
INSERT INTO StoreProduct  (StoreId,ProductId,PurchasedOty,Month,Year) 
			VALUES ('3','FRNCHFR','300','May','2013');
INSERT INTO StoreProduct  (StoreId,ProductId,PurchasedOty,Month,Year) 
			VALUES ('2','CKBURGR','450','May','2013');
INSERT INTO StoreProduct  (StoreId,ProductId,PurchasedOty,Month,Year) 
			VALUES ('4','VGBRITO','120','May','2013');
INSERT INTO StoreProduct  (StoreId,ProductId,PurchasedOty,Month,Year)
			VALUES ('5','CKSALAD','200','May','2013');
			
Print '5 records have been inserted into StoreProduct table'

COMMIT;
-----------------------------------------------------------------------------------

BEGIN TRANSACTION;
------------------ Managers
INSERT INTO Staff (StaffId,FirstName,LastName,Phone,Address,Gender,DateOfBirth,StoreId,PayLevel) 
			VALUES ('101','Sydnee','Sosa','(61) 4859 5645','P.O. Box 883, 6002 Purus Rd.','F','1975-11-18','1','E');
INSERT INTO Staff (StaffId,FirstName,LastName,Phone,Address,Gender,DateOfBirth,StoreId,PayLevel) 
			VALUES ('102','Iona','Mcintosh','(61) 4093 5436','Ap #196-3058 Porttitor Avenue','F','1972-06-20','4','E');
INSERT INTO Staff (StaffId,FirstName,LastName,Phone,Address,Gender,DateOfBirth,StoreId,PayLevel) 
			VALUES ('103','Brenna','Blackwell','(61) 4250 8994','Ap #134-9133 Mollis. Street','M','1968-12-30','2','E');
INSERT INTO Staff (StaffId,FirstName,LastName,Phone,Address,Gender,DateOfBirth,StoreId,PayLevel) 
			VALUES ('104','Nigel','Carlson','(61) 4567 0706','Ap #307-4863 Scelerisque Avenue','M','1974-04-03','3','E');
INSERT INTO Staff (StaffId,FirstName,LastName,Phone,Address,Gender,DateOfBirth,StoreId,PayLevel) 
			VALUES ('105','Maite','Daugherty','(61) 4439 1098','Ap #812-6454 Eros Road','F','1967-05-17','5','E');
			
------------------ Assistant Managers			
INSERT INTO Staff (StaffId,FirstName,LastName,Phone,Address,Gender,DateOfBirth,StoreId,PayLevel) 
			VALUES ('106','Grace','Cherry','(61) 4444 2788','106 Gravida Rd.','F','1982-06-09','3','D');
INSERT INTO Staff (StaffId,FirstName,LastName,Phone,Address,Gender,DateOfBirth,StoreId,PayLevel) 
			VALUES ('107','Roary','Glenn','(61) 4070 7444','8239 Eu Rd.','M','1980-10-14','2','D');
------------------- Other Employees			
INSERT INTO Staff (StaffId,FirstName,LastName,Phone,Address,Gender,DateOfBirth,DaysAvailable,TimesAvailable,StoreId,PayLevel) 
			VALUES ('108','Damon','Molina','(61) 4249 1346','P.O. Box 440, 4369 Eleifend Rd.','F','1983-03-12','Wednesday','12:00','1','C');
INSERT INTO Staff (StaffId,FirstName,LastName,Phone,Address,Gender,DateOfBirth,DaysAvailable,TimesAvailable,MentorId,MentorName,StoreId,PayLevel) 
			VALUES ('109','Geraldine','Pate','(61) 4562 1871','Ap #488-5404 Luctus St.','M','1990-09-21','Monday','14:00','108','Damon Molina','1','B');
INSERT INTO Staff (StaffId,FirstName,LastName,Phone,Address,Gender,DateOfBirth,DaysAvailable,TimesAvailable,StoreId,PayLevel) 
			VALUES ('110','Whilemina','Fry','(61) 4070 6271','9823 Vehicula Av.','F','1985-05-17','Monday','10:00','2','C');
INSERT INTO Staff (StaffId,FirstName,LastName,Phone,Address,Gender,DateOfBirth,DaysAvailable,TimesAvailable,MentorId,MentorName,StoreId,PayLevel) 
			VALUES ('111','Jarrod','Head','(61) 4137 8032','3718 Natoque Street','F','1989-07-01','Saturday','16:00','110','Whilemina Fry','2','A');
INSERT INTO Staff (StaffId,FirstName,LastName,Phone,Address,Gender,DateOfBirth,DaysAvailable,TimesAvailable,StoreId,PayLevel) 
			VALUES ('112','Hannah','Schneider','(61) 4117 2507','P.O. Box 871, 1032 Ut Ave','F','1979-07-02','Friday','10:00','3','C');
INSERT INTO Staff (StaffId,FirstName,LastName,Phone,Address,Gender,DateOfBirth,DaysAvailable,TimesAvailable,MentorId,MentorName,StoreId,PayLevel) 
			VALUES ('113','Ivor','Delgado','(61) 4367 4351','815-4447 Felis Road','M','1984-07-30','Tuesday','13:00','112','Hannah Schneider','3','B');
INSERT INTO Staff (StaffId,FirstName,LastName,Phone,Address,Gender,DateOfBirth,DaysAvailable,TimesAvailable,StoreId,PayLevel) 
			VALUES ('114','Karly','Jackson','(61) 4998 4139','146-6164 Aliquet. St.','M','1980-06-27','Tuesday','16:00','4','B');
INSERT INTO Staff (StaffId,FirstName,LastName,Phone,Address,Gender,DateOfBirth,DaysAvailable,TimesAvailable,MentorId,MentorName,StoreId,PayLevel) 
			VALUES ('115','Zeph','Stokes','(61) 4149 5337','623-2365 Nulla. Road','F','1991-01-27','Monday','10:00','114','Karly Jackson','4','A');
INSERT INTO Staff (StaffId,FirstName,LastName,Phone,Address,Gender,DateOfBirth,DaysAvailable,TimesAvailable,StoreId,PayLevel) 
			VALUES ('116','Jemima','Burks','(61) 4904 3727','2796 Aliquam Av.','M','1981-12-14','Tuesday','10:00','5','C');
INSERT INTO Staff (StaffId,FirstName,LastName,Phone,Address,Gender,DateOfBirth,DaysAvailable,TimesAvailable,MentorId,MentorName,StoreId,PayLevel) 
			VALUES ('117','Kylie','Vincent','(61) 4702 4205','5684 Quam St.','F','1988-08-07','Saturday','16:00','116','Jemima Burks','5','B');
			
Print '17 records have been inserted into Staff table'

COMMIT;
-----------------------------------------------------------------------------------

BEGIN TRANSACTION;

INSERT INTO Shift (ShiftId,ShiftDate,StartingTime,EndingTime,StoreId,StaffId) 
			VALUES ('123','2013-05-10','08:00','19:00','1','108');
INSERT INTO Shift (ShiftId,ShiftDate,StartingTime,EndingTime,StoreId,StaffId) 
			VALUES ('100','2013-05-10','09:30','19:30','1','109');
INSERT INTO Shift (ShiftId,ShiftDate,StartingTime,EndingTime,StoreId,StaffId) 
			VALUES ('178','2013-05-11','08:00','19:00','2','110');
INSERT INTO Shift (ShiftId,ShiftDate,StartingTime,EndingTime,StoreId,StaffId) 
			VALUES ('128','2013-05-11','09:00','20:00','2','111');
INSERT INTO Shift (ShiftId,ShiftDate,StartingTime,EndingTime,StoreId,StaffId) 
			VALUES ('186','2013-05-12','08:30','20:00','3','112');
INSERT INTO Shift (ShiftId,ShiftDate,StartingTime,EndingTime,StoreId,StaffId) 
			VALUES ('105','2013-05-12','09:00','20:00','3','113');
INSERT INTO Shift (ShiftId,ShiftDate,StartingTime,EndingTime,StoreId,StaffId) 
			VALUES ('167','2013-05-13','08:00','21:00','4','114');
INSERT INTO Shift (ShiftId,ShiftDate,StartingTime,EndingTime,StoreId,StaffId) 
			VALUES ('103','2013-05-13','08:00','20:00','4','115');
INSERT INTO Shift (ShiftId,ShiftDate,StartingTime,EndingTime,StoreId,StaffId) 
			VALUES ('177','2013-05-14','09:00','20:30','5','116');
INSERT INTO Shift (ShiftId,ShiftDate,StartingTime,EndingTime,StoreId,StaffId) 
			VALUES ('113','2013-05-14','08:30','21:00','5','117');



Print '10 records have been inserted into Shift table'

COMMIT;
-----------------------------------------------------------------------------------