SELECT s.LastName + ', ' + s.FirstName + 'does not mentor other staff' As 'Staff Non - Mentors' 
FROM Staff as s Left outer join  Staff as m
ON m.MentorId = s.StaffId
WHERE m.MentorId IS NULL