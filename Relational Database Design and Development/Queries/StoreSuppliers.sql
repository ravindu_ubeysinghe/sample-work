SELECT s.StoreName,s.ManagerName,sup.BusinessName,sup.ContactPersonName,sup.ContactPersonPhone
FROM   Store As s Inner Join StoreProduct As sp
ON     s.StoreId = sp.StoreId
	   Inner Join Supplier AS sup
ON     sp.ProductId = sup.ProductId;