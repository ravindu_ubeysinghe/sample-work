
SELECT TOP (3) s.StoreName , s.ManagerName , (sp.PurchasedOty * p.ProductPrice) AS 'Total Purchased' 
FROM Store AS s Inner Join StoreProduct AS sp
ON s.StoreId = sp.StoreId
	Inner join Product AS p
ON sp.ProductId = p.ProductId
ORDER BY 'Total Purchased'