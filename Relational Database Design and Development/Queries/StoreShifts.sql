SELECT sto.StoreName , sto.ManagerName , s.ShiftDate AS 'Shift Date' ,s.StartingTime AS 'Shift Start',s.EndingTime AS 'Shift End',
	   st.FirstName + st.LastName AS 'Name of Staff'
FROM   Store as sto Inner join Shift AS s
ON	   sto.StoreId = s.StoreId 
	   Inner Join Staff AS st
ON     s.StaffId = st.StaffId
ORDER BY s.ShiftDate,s.StartingTime;
	   
	   
	   