--The created database is going to be used now

USE master;

/* The following step will check whether the database exists and if it exists
   it will be droped */

IF DB_ID ('FastAndTastyFoods') IS NOT NULL
   BEGIN
       PRINT 'Database exists - it has been droped now.';
       DROP DATABASE FastAndTastyFoods;
   END

GO

CREATE DATABASE FastAndTastyFoods;

GO

USE FastAndTastyFoods;

GO
--********************************************************************
--Create Pay Table

CREATE TABLE Pay
 ( PayLevel        CHAR    (1) NOT NULL,
   PayLevelName    VARCHAR (50) NOT NULL,
   HourlySalary    MONEY    NOT NULL,
   Supperannuation CHAR    (3) NOT NULL,
   CONSTRAINT pay_level_pk PRIMARY KEY (PayLevel));

print 'Pay table has been created'

--********************************************************************
--Create Product Table

CREATE TABLE Product
 ( ProductId           CHAR     (7) NOT NULL,
   ProductName         VARCHAR  (20) NOT NULL,
   ProductPrice        MONEY     NOT NULL,
   ServingSize         VARCHAR  (7) NOT NULL,
   ProductDescription  VARCHAR  (50),
   CaloriesPerServe    NUMERIC  NOT NULL,
   Fat			       VARCHAR  (6),
   Protein			   VARCHAR  (6),
   SugarContent		   VARCHAR  (6), 
   FiberContent		   VARCHAR  (20),
   MainVitamin		   VARCHAR  (20),
   CarbohydrateContent VARCHAR  (20),
   
   CONSTRAINT product_id_pk PRIMARY KEY (ProductId));

print 'Product table has been created'

--********************************************************************
--Create Supplier Table

CREATE TABLE Supplier
 ( SupplierId         CHAR       (7)NOT NULL,
   BusinessName       VARCHAR    (30) NOT NULL,
   ABN                NUMERIC    (11) NOT NULL,
   ProductId          CHAR       (7) NOT NULL,
   ContactPersonName  VARCHAR    (20) NOT NULL,
   ContactPersonPhone VARCHAR    (14),
   Address			  VARCHAR    (50) NOT NULL,
   CONSTRAINT supplier_id_pk PRIMARY KEY (SupplierId),
   CONSTRAINT product_supplier_fk FOREIGN KEY (ProductId) REFERENCES Product(ProductId));

print 'Supplier table has been created'

--********************************************************************
--Create Store Table

CREATE TABLE Store
 ( StoreId           INT          NOT NULL,
   StoreName         VARCHAR      (20) NOT NULL,
   Location			 VARCHAR      (30) NOT NULL,
   Address           VARCHAR      (50) NOT NULL,
   OpeningHours      CHAR         (13) NOT NULL,
   ManagerId         INT          NOT NULL,
   ManagerName		 VARCHAR	  (20) NOT NULL,
   
   CONSTRAINT store_id_pk PRIMARY KEY (StoreId));

print 'Store table has been created'

--********************************************************************
--Create StoreProduct Table

CREATE TABLE StoreProduct
 ( StoreId           INT          NOT NULL,
   ProductId         CHAR     (7) NOT NULL,
   PurchasedOty		 INT,
   Month			 VARCHAR  (9) NOT NULL,
   Year				 CHAR     (4) NOT NULL,
    
   CONSTRAINT store_product_pk PRIMARY KEY (StoreId, ProductId),
   CONSTRAINT store_product_fk FOREIGN KEY (StoreId) REFERENCES Store(StoreId),
   CONSTRAINT product_store_fk FOREIGN KEY (ProductId) REFERENCES Product(ProductId));

print 'StoreProduct table has been created'

--********************************************************************
--Create Staff Table

CREATE TABLE Staff
 ( StaffId           INT          NOT NULL,
   FirstName         VARCHAR      (20) NOT NULL,
   LastName          VARCHAR      (20) NOT NULL,
   Phone			 VARCHAR	  (14),          
   Address           VARCHAR      (50) NOT NULL,
   Gender            CHAR         (1),
   DateOfBirth       DATE,
   DaysAvailable     CHAR         (9) DEFAULT 'Monday',
   TimesAvailable    TIME,
   MentorId          INT,
   MentorName		 VARCHAR      (40),
   StoreId           INT          NOT NULL,
   PayLevel          CHAR         (1)  NOT NULL,  
   
   CONSTRAINT staff_id_pk PRIMARY KEY (StaffId),
   CONSTRAINT mentor_fk FOREIGN KEY (MentorId) REFERENCES Staff(StaffId),
   CONSTRAINT store_staff_fk FOREIGN KEY (StoreId) REFERENCES Store(StoreId),
   CONSTRAINT pay_staff_fk FOREIGN KEY (PayLevel) REFERENCES Pay(PayLevel));

print 'Staff table has been created'

--********************************************************************
--Create Shift Table

CREATE TABLE Shift
 ( ShiftId           INT      NOT NULL,
   ShiftDate         DATE     NOT NULL  DEFAULT GETDATE(),
   StartingTime		 TIME     NOT NULL  DEFAULT '08:00:00',
   EndingTime		 TIME	  NOT NULL  DEFAULT '21:00:00',
   StoreId           INT      NOT NULL,
   StaffId           INT      NOT NULL,
    
   CONSTRAINT shift_id_pk PRIMARY KEY (ShiftId),
   CONSTRAINT store_shift_fk FOREIGN KEY (StoreId) REFERENCES Store(StoreId),
   CONSTRAINT staff_shift_fk FOREIGN KEY (StaffId) REFERENCES Staff(StaffId));

print 'Shift table has been created'